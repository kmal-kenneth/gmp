import 'package:flutter/material.dart';

// const double opacity = .4;
// const Color prymaryOpacity = Color.fromRGBO(6, 196, 184, opacity);
// const Color segundaryOpacity = Color.fromRGBO(42, 217, 142, opacity);
// const LinearGradient linearGradientOpacity = LinearGradient(
//   colors: <Color>[prymaryOpacity, segundaryOpacity],
// );

const Color prymary = Color.fromRGBO(6, 196, 184, 1);
const Color segundary = Color.fromRGBO(42, 217, 142, 1);
const Color normalText = Color.fromRGBO(158, 162, 175, 1);
const Color headerText = Color.fromRGBO(53, 56, 71, 1);

const LinearGradient linearGradient = LinearGradient(
  colors: <Color>[prymary, segundary],
);

final Shader linearGradientShader =
    linearGradient.createShader(Rect.fromLTWH(0.0, 0.0, 200.0, 70.0));
