import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/constant.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/provider/models/provider_model.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:my_app_stacked/ui/views/startup/startup_viewmodel.dart';
import 'package:my_app_stacked/widgets/circular_progress_bar.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class StartupView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final int numberOfArticles = 3;
    final int padding = 14;

    final double widthScreen = MediaQuery.of(context).size.width;
    final double widthItem = widthScreen / numberOfArticles - padding;
    final double heigthItem = widthItem * 4 / 3;
    // final double widthItemCategory = widthScreen / numberOfArticles - padding;
    // final double heigthItemCategoy = widthItemCategory * 4 / 3;

    return ViewModelBuilder<StartupViewModel>.reactive(
      builder: (context, model, child) => Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            leading: CloseButton(
              onPressed: () {
                SystemNavigator.pop();
              },
            ),
            actions: [
              FlatButton.icon(
                onPressed: () => model.isBusy == true ? null : model.refresh(),
                icon: Icon(
                  Icons.refresh,
                  color: normalText,
                ),
                label: Text(
                  "Actualizar",
                  style: TextStyle(color: normalText),
                ),
              ),
            ],
          ),
          body: ListView(
            padding: EdgeInsets.symmetric(horizontal: 12, vertical: 24),
            children: [
              _PreviewArticles(
                title: "Artículos para reabastecer",
                articles: model.shortageArticles,
                loading: model.loadingShortageArticles,
                onTapHeader: model.showAllArticlesShortage,
                widthItem: widthItem,
                heigthItem: heigthItem,
                numberOfArticles: model.numberOfShortageArticles,
                refresh: model.refresh,
              ),
              SizedBox(
                height: 12,
              ),
              _PreviewArticles(
                title: "Artículos",
                articles: model.articles,
                loading: model.loadingArticles,
                onTapHeader: model.showAllArticles,
                widthItem: widthItem,
                heigthItem: heigthItem,
                numberOfArticles: model.numberOfArticles,
                refresh: model.refresh,
              ),
              SizedBox(
                height: 12,
              ),
              _PreviewArticles(
                title: "Proveedores",
                providers: model.providers,
                loading: model.loadingProviders,
                onTapHeader: model.showAllProviders,
                widthItem: widthItem,
                heigthItem: heigthItem,
                numberOfArticles: model.numberOfProviders,
                refresh: model.refresh,
              ),
              // _CategoriesArticles(
              //   title: "Categorías ",
              //   model: model,
              //   heigthItem: heigthItemCategoy,
              // )
            ],
          )),
      viewModelBuilder: () => StartupViewModel(),
    );
  }
}

class _PreviewArticles extends StatelessWidget {
  final String title;

  final double heigthItem, widthItem;
  final List<ArticleModel> articles;
  final List<ProviderModel> providers;
  final bool loading;
  final int numberOfArticles;
  final Function refresh, onTapHeader;

  const _PreviewArticles(
      {Key key,
      @required this.heigthItem,
      @required this.widthItem,
      @required this.title,
      this.articles,
      this.providers,
      @required this.onTapHeader,
      @required this.loading,
      @required this.refresh,
      @required this.numberOfArticles})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Column column = Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        _HeaderSectionArticles(
          onPressed: () => onTapHeader(),
          title: title,
        ),
        _ListArticlesItems(
          heigthItem: heigthItem,
          widthItem: widthItem,
          numberOfArticles: numberOfArticles,
          articles: articles ?? null,
          providers: providers ?? null,
          refresh: refresh,
        ),
      ],
    );

    Column loading = Column(
      children: [
        _HeaderSectionArticles(
          onPressed: null,
          title: title,
        ),
        SizedBox(
            height: heigthItem, child: Center(child: CircularProgressBar()))
      ],
    );

    return this.loading ? loading : column;
  }
}

class _ListArticlesItems extends StatelessWidget {
  const _ListArticlesItems({
    Key key,
    @required this.heigthItem,
    @required this.widthItem,
    @required this.numberOfArticles,
    this.articles,
    this.providers,
    @required this.refresh,
  }) : super(key: key);

  final double heigthItem, widthItem;
  final int numberOfArticles;
  final List<ArticleModel> articles;
  final List<ProviderModel> providers;
  final Function refresh;

  @override
  Widget build(BuildContext context) {
    final NavigationService _navigationService = locator<NavigationService>();
    var articlesList = ListView();
    if (articles != null) {
      articlesList = ListView(
        scrollDirection: Axis.horizontal,
        children: [
          for (ArticleModel item in articles)
            Container(
              margin: EdgeInsets.symmetric(horizontal: 3),
              child: ArticleVerticlaItem(
                onTap: () async {
                  await _navigationService.navigateTo(Routes.articleView,
                      arguments: ArticleViewArguments(article: item));
                },
                width: widthItem,
                quantity: item.quantity.toString(),
                label: item.name,
                imageUrl: item.images.length < 1 ? "" : item.images.first.url,
              ),
            ),
        ],
      );
    }

    var providersList = ListView();
    if (providers != null) {
      providersList = ListView(
        scrollDirection: Axis.horizontal,
        children: [
          for (var item in providers)
            Container(
              margin: EdgeInsets.symmetric(horizontal: 3),
              child: ArticleVerticlaItem(
                onTap: () async {
                  await _navigationService.navigateTo(Routes.providerView,
                      arguments: ProviderViewArguments(provider: item));

                  refresh();
                },
                width: widthItem,
                label: item.name,
                imageUrl: item.images.length < 1 ? "" : item.images.first.url,
              ),
            ),
        ],
      );
    }
    return SizedBox(
        height: heigthItem,
        child: articles != null
            ? articlesList
            : providers != null
                ? providersList
                : SizedBox());
  }
}

class _HeaderSectionArticles extends StatelessWidget {
  const _HeaderSectionArticles({
    Key key,
    this.onPressed,
    @required this.title,
    this.fontSize = 16,
  }) : super(key: key);

  final Function onPressed;
  final String title;
  final double fontSize;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 48,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Text(
              this.title,
              style: TextStyle(
                  color: headerText,
                  fontWeight: FontWeight.w600,
                  fontSize: this.fontSize),
            ),
          ),
          FlatButton(
            onPressed: onPressed,
            child: Icon(
              Icons.more_horiz,
              color: Colors.black45,
            ),
          )
        ],
      ),
    );
  }
}
