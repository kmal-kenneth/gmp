import 'package:flutter/cupertino.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/provider/models/provider_model.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class StartupViewModel extends BaseViewModel {
  bool _loadingShortageArticles = true;
  bool _loadingArticles = true;
  bool _loadingProviders = true;
  final DialogService _dialogService = locator<DialogService>();

  List<ArticleModel> _shortageArticles = List<ArticleModel>();
  List<ArticleModel> _articles = List<ArticleModel>();
  List<ProviderModel> _providers = List<ProviderModel>();

  final ArticleApiService _apiService = ArticleApiService();
  final ProviderRepositoryService _providerRepository =
      ProviderRepositoryService();
  StartupViewModel() {
    refresh();
  }
  List<ArticleModel> get articles => this._articles;
  bool get loadingArticles => this._loadingArticles;
  bool get loadingProviders => this._loadingProviders;

  bool get loadingShortageArticles => this._loadingShortageArticles;

  int get numberOfArticles => this._articles.length;
  int get numberOfProviders => this._providers.length;
  int get numberOfShortageArticles => this._shortageArticles.length;
  List<ProviderModel> get providers => this._providers;

  List<ArticleModel> get shortageArticles => this._shortageArticles;
  Function get showAllProviders => this._showAllProviders;

  fetchArticles() async {
    _loadingArticles = true;
    notifyListeners();

    var result = await _apiService.fetchArticlesLimit(
        limit: 3); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo obtener los artículos.',
        description: result,
      );
    }

    if (result is List<ArticleModel>) {
      _articles = result;
    }

    _loadingArticles = false;
    notifyListeners();
  }

  fetchShortageArticles() async {
    _loadingShortageArticles = true;
    notifyListeners();

    var result = await _apiService.fetchShortageArticlesLimit(
        limit: 3); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo obtener los artículos para reabastecer.',
        description: result,
      );
    }

    if (result is List<ArticleModel>) {
      _shortageArticles = result;
    }

    _loadingShortageArticles = false;
    notifyListeners();
  }

  fetchProviders() async {
    _loadingProviders = true;
    notifyListeners();

    var result = await _providerRepository.fetchProvidersLimit(
        limit: 3); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo obtener los proveedores.',
        description: result,
      );
    }

    if (result is List<ProviderModel>) {
      _providers = result;
    }

    _loadingProviders = false;
    notifyListeners();
  }

  refresh() {
    fetchShortageArticles();
    fetchArticles();
    fetchProviders();
    notifyListeners();
  }

  showAllArticlesShortage() async {
    final NavigationService _navigationService = locator<NavigationService>();

    await _navigationService.navigateTo(Routes.articleListView,
        arguments: ArticleListViewArguments(shortage: true));

    refresh();
  }

  showAllArticles() async {
    final NavigationService _navigationService = locator<NavigationService>();

    await _navigationService.navigateTo(
      Routes.articleListView,
    );

    refresh();
  }

  _showAllProviders() async {
    final NavigationService _navigationService = locator<NavigationService>();

    await _navigationService.navigateTo(
      Routes.providerListView,
    );

    refresh();
  }

  showArticle({@required ArticleModel article}) async {
    final NavigationService _navigationService = locator<NavigationService>();

    await _navigationService.navigateTo(Routes.articleView,
        arguments: ArticleViewArguments(article: article));
    refresh();
  }

  showProvider({@required ProviderModel provider}) async {
    final NavigationService _navigationService = locator<NavigationService>();

    await _navigationService.navigateTo(Routes.providerView,
        arguments: ProviderViewArguments(provider: provider));
    refresh();
  }
}
