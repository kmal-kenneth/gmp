import 'package:auto_route/auto_route.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class HomeViewModel extends BaseViewModel {
  final ArticleApiService apiService = ArticleApiService();

  var articles;

  HomeViewModel() {
    articles = List<ArticleModel>();
  }

  navigateToArticleEditView({@required ArticleModel article}) {
    _navigationService.navigateTo(Routes.articleEditView,
        arguments: ArticleEditViewArguments(article: article));
  }

  final NavigationService _navigationService = locator<NavigationService>();

  void requestMoreData() => apiService.requestMoreData(shortageArticles: true);

  void listenToArticles() {
    apiService.articlesRealTime(shortageArticles: true).listen((result) {
      List<ArticleModel> updatedArticles = result;
      print(updatedArticles.length);
      if (updatedArticles != null) {
        articles = updatedArticles;
        notifyListeners();
      }
    });
  }
}
