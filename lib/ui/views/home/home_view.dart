import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/ui/views/home/home_viewmodel.dart';
import 'package:stacked/stacked.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<HomeViewModel>.reactive(
      viewModelBuilder: () => HomeViewModel(),
      onModelReady: (model) => model.listenToArticles(),
      builder: (context, model, child) => Scaffold(
        appBar: AppBar(
          title: Text(
            '${model.articles.length} - Artículos a abastecer',
          ),
        ),
        body: model.articles.isEmpty
            ? Center(
                child: Text("No hay artículos que abastecer."),
              )
            : GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 3 / 4,
                    crossAxisCount: 3,
                    crossAxisSpacing: 4.0,
                    mainAxisSpacing: 4.0),
                itemCount: model.articles.length,
                itemBuilder: (context, index) => GestureDetector(
                      onTap: () {
                        model.navigateToArticleEditView(
                            article: model.articles[index]);
                      },
                      child: SizedBox(
                        child: Card(
                          elevation: 2,
                          child: _Image(
                            article: model.articles[index],
                          ),
                        ),
                      ),
                    )),
      ),
    );
  }
}

class _Image extends StatelessWidget {
  /// Article to show
  final ArticleModel article;

  /// Blur control
  final sigma;

  /// Opacity of footer
  final opacity;

  /// Font size of footer
  final fontSize;

  /// Picture with quantity for article
  /// @required [article]
  const _Image({
    Key key,
    this.article,
    this.sigma = 1.1,
    this.opacity = 0.6,
    this.fontSize = 12.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Stack(
          children: [
            article.images.isNotEmpty
                ? CachedNetworkImage(
                    width: double.infinity,
                    height: double.infinity,
                    fit: BoxFit.cover,
                    imageUrl: article.images[0].url,
                    placeholder: (context, url) =>
                        ImagePlaceHolder(icon: Icons.image),
                    errorWidget: (context, url, error) =>
                        ImagePlaceHolder(icon: Icons.broken_image),
                  )
                : ImagePlaceHolder(icon: Icons.image_not_supported),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 24,
                width: double.infinity,
                padding: EdgeInsets.symmetric(horizontal: 4),
                decoration:
                    BoxDecoration(color: Colors.black.withOpacity(opacity)),
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
                    child: Center(
                        child: Text(
                      article.name.toString(),
                      maxLines: 1,
                      overflow: TextOverflow.clip,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                          fontWeight: FontWeight.w500),
                    )),
                  ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(opacity),
                    borderRadius: BorderRadius.circular(6)),
                child: ClipRect(
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
                    child: Text(
                      article.quantity.toString(),
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
