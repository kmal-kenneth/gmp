// To parse this JSON data, do
//
//     final article = articleFromMap(jsonString);

import 'dart:convert';

import 'package:my_app_stacked/image/image.dart';

ArticleModel articleFromMap(String str, id) =>
    ArticleModel.fromMap(json.decode(str), id);

String articleToMap(ArticleModel data) => json.encode(data.toMap());

class ArticleModel {
  String id;

  bool automaticallyNotifyShortage;
  bool shortage;
  String description;
  int maximumAmountNotifyShortage;
  String name;
  int quantity;
  List<ImageModel> images;
  ArticleModel(
      {this.id,
      this.automaticallyNotifyShortage = false,
      this.description,
      this.maximumAmountNotifyShortage,
      this.name,
      this.quantity,
      this.images,
      this.shortage = false}) {
    automaticallyNotifyShortage = false;
    images = List<ImageModel>();
  }

  factory ArticleModel.fromMap(Map<String, dynamic> json, String id) {
    var list = json['images'] as List;

    var article = ArticleModel(
      id: id ?? '',
      shortage: json["shortage"],
      description: json["description"],
      maximumAmountNotifyShortage: json["maximumAmountNotifyShortage"],
      name: json["name"],
      quantity: json["quantity"],
    );

    article.images = list.map((i) => ImageModel.fromMap(i)).toList();
    article.automaticallyNotifyShortage = json["automaticallyNotifyShortage"];
    article.shortage = json["shortage"];
    return article;
  }

  Map<String, dynamic> toMap() => {
        "automaticallyNotifyShortage": automaticallyNotifyShortage,
        "shortage": shortage,
        "description": description,
        "maximumAmountNotifyShortage": maximumAmountNotifyShortage,
        "name": name,
        "quantity": quantity,
        "images": images.map((image) => image.toMap()).toList()
      };
}
