import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app_stacked/image/widgets/image_place_holder.dart';

class ArticleVerticlaItem extends StatelessWidget {
  /// Article to show

  /// Blur control
  final sigma;

  /// Opacity of footer
  final opacity;

  /// Font size of footer
  final fontSize;

  final String quantity;
  final label;
  final imageUrl;

  final width;
  final double borderRadius;

  final Function onTap;

  /// Picture with quantity for article
  /// @required [article]
  const ArticleVerticlaItem(
      {Key key,
      this.opacity = 0.6,
      this.sigma = 1.1,
      this.fontSize = 12.0,
      this.quantity = "",
      this.label = "",
      this.imageUrl = "",
      this.borderRadius = 6.0,
      this.onTap,
      this.width = 64.0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onTap is Function ? onTap() : null,
      child: Container(
        width: this.width,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(borderRadius),
          child: AspectRatio(
            aspectRatio: 3 / 4,
            child: Stack(
              children: [
                imageUrl.isNotEmpty
                    ? CachedNetworkImage(
                        width: double.infinity,
                        height: double.infinity,
                        fit: BoxFit.cover,
                        imageUrl: imageUrl,
                        placeholder: (context, url) =>
                            ImagePlaceHolder(icon: Icons.image),
                        errorWidget: (context, url, error) =>
                            ImagePlaceHolder(icon: Icons.broken_image),
                      )
                    : ImagePlaceHolder(icon: Icons.image_not_supported),
                _Label(
                    opacity: opacity,
                    sigma: sigma,
                    label: label,
                    fontSize: fontSize),
                quantity.isEmpty
                    ? SizedBox()
                    : _QuantityIndicator(
                        opacity: opacity,
                        sigma: sigma,
                        quantity: quantity,
                        fontSize: fontSize)
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Label extends StatelessWidget {
  const _Label({
    Key key,
    @required this.opacity,
    @required this.sigma,
    @required this.label,
    @required this.fontSize,
  }) : super(key: key);

  final opacity;
  final sigma;
  final label;
  final fontSize;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 24,
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 4),
        decoration: BoxDecoration(color: Colors.black.withOpacity(opacity)),
        child: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
            child: Center(
                child: Text(
              label,
              maxLines: 1,
              overflow: TextOverflow.clip,
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize,
                  fontWeight: FontWeight.w500),
            )),
          ),
        ),
      ),
    );
  }
}

class _QuantityIndicator extends StatelessWidget {
  const _QuantityIndicator({
    Key key,
    @required this.opacity,
    @required this.sigma,
    @required this.quantity,
    @required this.fontSize,
  }) : super(key: key);

  final opacity;
  final sigma;
  final quantity;
  final fontSize;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(opacity),
            borderRadius: BorderRadius.circular(6)),
        child: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
            child: Text(
              quantity,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
  }
}
