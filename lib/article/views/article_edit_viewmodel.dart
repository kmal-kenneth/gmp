import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/article/repositories/article_api_service.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/image/image_selector.dart';
import 'package:my_app_stacked/image/repository/image_repository.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ArticleEditViewModel extends BaseViewModel {
  final ArticleApiService _apiService = locator<ArticleApiService>();
  final DialogService _dialogService = locator<DialogService>();

  ArticleModel article;
  ArticleModel articleTemp;

  bool loading = true;

  // String title;

  final ImageSelector _imageSelector = locator<ImageSelector>();

  final ImageRepository _cloudStorageService = locator<ImageRepository>();
  List<ImageWrapper> images;
  List<ImageWrapper> deleteImages;
  ImageWrapper selectedImage;

  ArticleEditViewModel({this.article}) {
    init();
    if (article.id != null) {
      fetchArticle();
    } else {
      loading = false;
      notifyListeners();
    }
  }
  fetchArticle() async {
    loading = true;
    notifyListeners();

    var result = await _apiService
        .getArticleById(article.id); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo obtener el artículo.',
        description: result,
      );
    }

    if (result is ArticleModel) {
      article = result;
      init();
    }

    loading = false;
    notifyListeners();
  }

  init() {
    if (article == null) {
      article = ArticleModel();
      article.automaticallyNotifyShortage = false;
      // title = 'Crear artículo';
    } else {
      // title = 'Modificar artículo';
    }
    articleTemp = ArticleModel.fromMap(article.toMap(), article.id);

    images = List<ImageWrapper>();
    deleteImages = List<ImageWrapper>();

    addSevedImages(images: articleTemp.images);
  }

  ///Add a new article to persistend data
  Future addArticle() async {
    var result = await _apiService.addArticle(
        article: article); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo agregar el artículo.',
        description: result,
      );
    }
  }

  ///Add saved images to [images] for show in view
  void addSevedImages({@required List<ImageModel> images}) {
    if (articleTemp.images != null && articleTemp.images.isNotEmpty) {
      articleTemp.images.forEach((image) {
        this.images.add(ImageWrapper(saved: true, image: image));
      });

      notifyListeners();
    }
  }

  /// Delete saved images of persistend data
  deleteSavedImages() {
    deleteImages.forEach((imageWrap) async {
      deleteImage(imageWrap: imageWrap);
      article.images.remove(imageWrap.image);
    });
  }

  ///Move image in the [images]
  void moveImage({@required int oldIndex, @required int newIndex}) {
    final ImageWrapper temp = images.removeAt(oldIndex);
    images.insert(newIndex, temp);

    notifyListeners();
  }

  /// Get image form galery or camera
  Future pickImage({@required ImageSource source}) async {
    var tempImage = await _imageSelector.selectImage(source: source);
    if (tempImage != null) {
      var image = ImageWrapper(
        saved: false,
        file: tempImage,
      );

      images.add(image);

      notifyListeners();
    }
  }

  /// Save the actual article
  ///
  /// If [tempArticle.id.isNotEmpty] update the article.
  /// But if [tempArticle.id] is empty add the article.
  save() async {
    setBusy(true);
    notifyListeners();
    article = articleTemp;

    article.images.clear();

    for (var imageWrap in images) {
      if (imageWrap.saved == false) {
        article.images.add(await uploadImage(imageWrap: imageWrap));
      } else {
        article.images.add(imageWrap.image);
      }
    }

    if (article.automaticallyNotifyShortage) {
      if (article.quantity <= article.maximumAmountNotifyShortage) {
        article.shortage = true;
      } else {
        article.shortage = false;
      }
    } else {
      article.shortage = false;
    }

    if (article.id.isNotEmpty) {
      if (deleteImages.isNotEmpty) {
        await deleteSavedImages();
      }

      await updateArticle();
    } else {
      await addArticle();
    }

    setBusy(false);
    notifyListeners();
  }

  ///Set [articleTemp.automaticallyNotifyShortage] for show other part of the view
  setAutomaticallyNotifyShortage(value) {
    articleTemp.automaticallyNotifyShortage = value;
    notifyListeners();
  }

  ///Update the saved article in the persistend data
  Future<void> updateArticle() async {
    // We need to add the current userId
    var result = await _apiService.updateArticle(article: article);

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo modificar el artículo.',
        description: result,
      );
    }
  }

  ///Upload no saved images to firebase
  Future<ImageModel> uploadImage({ImageWrapper imageWrap}) async {
    CloudStorageResult storageResult;
    storageResult = await _cloudStorageService.uploadImage(
        imageToUpload: imageWrap.file, title: article.name);
    return ImageModel(
        name: storageResult.imageFileName, url: storageResult.imageUrl);
  }

  validateDescription(value) {
    var tempValue = value.trim();
    if (tempValue.isEmpty) {
      return 'Se requiere una descripción.';
    }
    return null;
  }

  ///Upload no saved images to firebase
  deleteImage({ImageWrapper imageWrap}) async {
    var storageResult =
        await _cloudStorageService.deleteImage(imageWrap.image.name);

    if (storageResult is String) {
      await _dialogService.showDialog(
        title: 'No se pudo obtener el artículo.',
        description: storageResult,
      );
    }
  }

  validateMaximunAmountNotifyShortage(value) {
    var tempValue = int.tryParse(value.trim());

    if (value.isEmpty) {
      return 'Se requiere una cantidad.';
    }

    if (tempValue == null) {
      return 'Debe ser un número.';
    } else if (tempValue < 0) {
      return 'Debe ser 0 ó mayor.';
    }

    return null;
  }

  validateName(value) {
    var tempValue = value.trim();
    if (tempValue.isEmpty) {
      return 'Se requiere un nombre.';
    }
    return null;
  }

  validateQuantity(value) {
    var tempValue = value.trim();

    if (tempValue.isEmpty) {
      return 'Se requiere una cantidad.';
    }

    if (int.tryParse(value) == null) {
      return 'Debe ser un número.';
    }
    return null;
  }
}
