import 'package:flutter/material.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/article/views/article_view_viewmodel.dart';
import 'package:my_app_stacked/image/widgets/my_image_cache.dart';
import 'package:stacked/stacked.dart';

import '../../constant.dart';

class ArticleView extends StatelessWidget {
  final ArticleModel _article;
  const ArticleView({ArticleModel article}) : _article = article;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ArticleViewViewmodel>.reactive(
      viewModelBuilder: () => ArticleViewViewmodel(article: _article),
      builder: (context, model, child) {
        final int numberOfArticles = 3;
        final int padding = 14;

        final double widthScreen = MediaQuery.of(context).size.width;
        final double widthItem = widthScreen / numberOfArticles - padding;
        final double heigthItem = widthItem * 3 / 4;
        // final double widthItemCategory = widthScreen / numberOfArticles - padding;
        // final double heigthItemCategoy = widthItemCategory * 4 / 3;

        return Scaffold(
          appBar: AppBar(
            leading: BackButton(onPressed: () => model.navigationBack()),
          ),
          body: Container(
            color: Colors.white,
            child: ListView(
              children: [
                MyImageCache(
                  url: model.urlFirstImage,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        model.article.name,
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: headerText),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Text(
                          model.article.description,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: normalText),
                        ),
                      ),
                      Divider(
                        thickness: 1.2,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Text(
                              model.quantity,
                              style: TextStyle(
                                  fontSize: 18,
                                  color: normalText,
                                  fontWeight: FontWeight.w500),
                            ),
                            model.article.shortage
                                ? Text(
                                    'Escaso',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: normalText,
                                        fontWeight: FontWeight.w500),
                                  )
                                : SizedBox()
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1.2,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Galería',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: headerText),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      SizedBox(
                        height: heigthItem,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            for (var item in model.images)
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 3),
                                child: InkWell(
                                  onTap: () {
                                    model.navigationtoImageViewer(
                                        select: item, images: model.images);
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(6),
                                    child: MyImageCache(
                                      url: item.image.url,
                                    ),
                                  ),
                                ),
                              )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
