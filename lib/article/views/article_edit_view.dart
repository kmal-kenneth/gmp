import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/article/views/article_edit_viewmodel.dart';
import 'package:my_app_stacked/constant.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:my_app_stacked/widgets/circular_progress_bar.dart';
import 'package:oktoast/oktoast.dart';
import 'package:reorderables/reorderables.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ArticleEditView extends StatelessWidget {
  final ArticleModel _article;
  const ArticleEditView({ArticleModel article}) : _article = article;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ArticleEditViewModel>.reactive(
      viewModelBuilder: () => ArticleEditViewModel(article: _article),
      builder: (context, model, child) => MyForm(model: model),
    );
  }
}

class MyForm extends StatefulWidget {
  final ArticleEditViewModel _model;

  const MyForm({
    Key key,
    @required ArticleEditViewModel model,
  })  : _model = model,
        super(key: key);

  @override
  _MyFormState createState() => _MyFormState();
}

class SaveButton extends StatelessWidget {
  final GlobalKey<FormState> _formKey;

  final ArticleEditViewModel model;
  const SaveButton({
    Key key,
    @required GlobalKey<FormState> formKey,
    @required this.model,
  })  : _formKey = formKey,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: model.isBusy == true
          ? null
          : () async {
              final form = _formKey.currentState;

              if (form.validate()) {
                form.save();

                await model.save();

                showToast(
                  model.articleTemp.id.isEmpty
                      ? "Se agrego exitosamente"
                      : "Se guardo exitosamente",
                  textStyle: TextStyle(fontSize: 16.0, color: Colors.white),
                  textPadding:
                      EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  position: ToastPosition.bottom,
                  backgroundColor: Colors.black.withOpacity(0.55),
                  radius: 100.0,
                );

                final NavigationService _navigationService =
                    locator<NavigationService>();

                _navigationService.back(result: true);
              }
            },
      child: model.isBusy == false
          ? Text(
              'Guardar',
              style: TextStyle(color: normalText),
            )
          : CircularProgressBar(
              size: 20,
            ),

      // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
    );
  }
}

class _InputAutomaticallyNotifyShortage extends StatelessWidget {
  final ArticleEditViewModel model;
  const _InputAutomaticallyNotifyShortage({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text('Notificar escasez automáticamente'),
        Switch(
            value: model.articleTemp.automaticallyNotifyShortage,
            onChanged: (bool newValue) =>
                model.setAutomaticallyNotifyShortage(newValue)),
      ],
    );
  }
}

class _InputDescription extends StatelessWidget {
  final ArticleEditViewModel model;
  const _InputDescription({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.newline,
      initialValue: model.articleTemp.description,
      decoration: InputDecoration(
        labelText: 'Descripción',
        hintText: 'Bolsa de 100 unidades',
      ),
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      maxLines: null,
      onSaved: (value) {
        model.articleTemp.description = value;
      },
      validator: (value) => model.validateDescription(value),
    );
  }
}

class _InputMaximumAmountNotifyShortage extends StatelessWidget {
  final ArticleEditViewModel model;
  const _InputMaximumAmountNotifyShortage({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: model.articleTemp.maximumAmountNotifyShortage != null
          ? model.articleTemp.maximumAmountNotifyShortage.toString()
          : '',
      enabled: model.articleTemp.automaticallyNotifyShortage,
      decoration: InputDecoration(
        labelText: 'Cantidad máxima para notificar escasez',
        hintText: '5',
      ),
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) {
        model.articleTemp.maximumAmountNotifyShortage = int.parse(value);
      },
      validator: (value) => model.validateMaximunAmountNotifyShortage(value),
    );
  }
}

class _InputQuantity extends StatelessWidget {
  final ArticleEditViewModel model;
  const _InputQuantity({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: model.articleTemp.quantity != null
          ? model.articleTemp.quantity.toString()
          : '',
      decoration: InputDecoration(
        labelText: 'Cantidad',
        hintText: '17',
      ),
      keyboardType: TextInputType.number,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.digitsOnly
      ],
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) {
        model.articleTemp.quantity = int.parse(value);
      },
      validator: (value) => model.validateQuantity(value),
    );
  }
}

class _MyFormState extends State<MyForm> {
  final _formKey = GlobalKey<FormState>();
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // title: Text(
        //   widget._model.title,
        //   style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
        // ),
        backgroundColor: Colors.white,
        leading: BackButton(),
        actions: [SaveButton(formKey: _formKey, model: widget._model)],
      ),
      body: widget._model.loading
          ? Center(
              child: CircularProgressBar(
                size: 48,
              ),
            )
          : Form(
              key: _formKey,
              autovalidateMode: AutovalidateMode.disabled,
              child: ListView(
                padding: EdgeInsets.all(16),
                children: <Widget>[
                  SizedBox(
                    height: 8,
                  ),
                  _ImagesList(
                    widget: widget,
                    navigationService: _navigationService,
                  ),
                  SizedBox(
                    height: widget._model.images.isEmpty ? null : 8,
                  ),
                  Center(child: _AddImage(widget: widget)),
                  SizedBox(
                    height: 24,
                  ),
                  _NameInput(
                    model: widget._model,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  _InputDescription(
                    model: widget._model,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  _InputQuantity(
                    model: widget._model,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  _InputAutomaticallyNotifyShortage(
                    model: widget._model,
                  ),
                  SizedBox(
                    height: 24,
                  ),
                  if (widget._model.articleTemp.automaticallyNotifyShortage)
                    _InputMaximumAmountNotifyShortage(
                      model: widget._model,
                    ),
                ],
              ),
            ),
    );
  }
}

class _ImagesList extends StatelessWidget {
  const _ImagesList({
    Key key,
    @required this.widget,
    @required NavigationService navigationService,
  })  : _navigationService = navigationService,
        super(key: key);

  final MyForm widget;
  final NavigationService _navigationService;

  @override
  Widget build(BuildContext context) {
    return ReorderableWrap(
      spacing: 4.0,
      runSpacing: 4.0,
      maxMainAxisCount: 4,
      children: [
        for (final item in widget._model.images)
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
            key: ValueKey(item),
            width: MediaQuery.of(context).size.width / 4 - 12,
            child: GestureDetector(
              onTap: () async {
                widget._model.deleteImages =
                    await _navigationService.navigateTo(Routes.imageViewer,
                        arguments: ImageViewerArguments(
                            image: item,
                            images: widget._model.images,
                            deleteAction: true));

                widget._model.notifyListeners();
              },
              child: AspectRatio(
                aspectRatio: 4 / 3,
                child: item.saved
                    ? CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: item.image.url,
                        placeholder: (context, url) =>
                            ImagePlaceHolder(icon: Icons.image),
                        errorWidget: (context, url, error) =>
                            ImagePlaceHolder(icon: Icons.broken_image),
                      )
                    : Image.file(
                        item.file,
                        fit: BoxFit.cover,
                      ),
              ),
            ),
          ),
      ],
      onReorder: (oldIndex, newIndex) {
        widget._model.moveImage(oldIndex: oldIndex, newIndex: newIndex);
      },
    );
  }
}

class _AddImage extends StatelessWidget {
  const _AddImage({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final MyForm widget;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // When we tap we call selectImage
      onTap: () {
        showModalBottomSheet(
            context: context,
            builder: (BuildContext bc) {
              return SafeArea(
                child: Container(
                  child: new Wrap(
                    children: <Widget>[
                      new ListTile(
                          leading: new Icon(Icons.photo_library),
                          title: new Text(
                            'Galería',
                            style: TextStyle(color: headerText),
                          ),
                          onTap: () {
                            widget._model
                                .pickImage(source: ImageSource.gallery);
                            Navigator.of(context).pop();
                          }),
                      new ListTile(
                        leading: new Icon(Icons.photo_camera),
                        title: new Text('Cámara'),
                        onTap: () {
                          widget._model.pickImage(source: ImageSource.camera);

                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ),
              );
            });
      },
      child: Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
        width: MediaQuery.of(context).size.width / 4 - 12,
        child: AspectRatio(
          aspectRatio: 4 / 3,
          child: ImagePlaceHolder(
            icon: Icons.add_a_photo,
          ),
        ),
      ),
    );
  }
}

class _NameInput extends StatelessWidget {
  final ArticleEditViewModel model;
  const _NameInput({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: model.articleTemp.name,
      decoration: InputDecoration(
        labelText: 'Nombre',
        hintText: 'Tornillos 1 pulgada',
      ),
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) {
        model.articleTemp.name = value.trim();
      },
      validator: (value) => model.validateName(value),
    );
  }
}
