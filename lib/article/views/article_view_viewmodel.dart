import 'package:auto_route/auto_route.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/image/models/image_model.dart';
import 'package:my_app_stacked/image/utils/image_wrapper.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ArticleViewViewmodel extends BaseViewModel {
  final NavigationService _navigation = locator<NavigationService>();

  ArticleModel _article;
  List<ImageWrapper> images;

  ArticleViewViewmodel({ArticleModel article}) {
    this._article = article;
    images = List<ImageWrapper>();

    addSevedImages(images: _article.images);
  }

  ///Add saved images to [images] for show in view
  void addSevedImages({@required List<ImageModel> images}) {
    if (_article.images != null && _article.images.isNotEmpty) {
      _article.images.forEach((image) {
        this.images.add(ImageWrapper(saved: true, image: image));
      });

      notifyListeners();
    }
  }

  ArticleModel get article => this._article;

  String get urlFirstImage =>
      this._article.images.isNotEmpty ? this._article.images.first.url : "";

  String get quantity => _article.automaticallyNotifyShortage
      ? "${_article.quantity}/${_article.maximumAmountNotifyShortage}"
      : "${_article.quantity}";

  set setArticle(ArticleModel article) {
    this._article = article;
  }

  navigationBack() {
    _navigation.back();
  }

  navigationtoImageViewer(
      {@required ImageWrapper select, @required List<ImageWrapper> images}) {
    _navigation.navigateTo(Routes.imageViewer,
        arguments: ImageViewerArguments(
            image: select, images: images, deleteAction: false));
  }
}
