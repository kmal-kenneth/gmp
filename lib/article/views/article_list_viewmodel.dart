import 'package:auto_route/auto_route.dart';
import 'package:my_app_stacked/article/article.dart';
import 'package:my_app_stacked/article/repositories/article_api_service.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ArticleListViewModel extends BaseViewModel {
  List<ArticleModel> articles;

  final NavigationService _navigationService = locator<NavigationService>();

  final ArticleApiService apiService = locator<ArticleApiService>();

  final DialogService _dialogService = locator<DialogService>();

  final bool shortage;

  ArticleListViewModel({this.shortage}) {
    articles = List<ArticleModel>();
  }

  addOne({@required ArticleModel article}) async {
    article.quantity += 1;

    if (article.automaticallyNotifyShortage) {
      if (article.quantity <= article.maximumAmountNotifyShortage) {
        article.shortage = true;
      } else {
        article.shortage = false;
      }
    } else {
      article.shortage = false;
    }

    var result = await apiService.updateArticle(
        article: article); // We need to add the current userId
    notifyListeners();

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo incrementar el artículo.',
        description: result,
      );
    }
  }

  deleteArticle({@required ArticleModel article}) async {
    DialogResponse response = await _dialogService.showConfirmationDialog(
      title: 'Desea eliminar ${article.name}?',
      description: 'Esta accion no se puede revertir.',
      confirmationTitle: 'Eliminar',
      dialogPlatform: DialogPlatform.Material,
      cancelTitle: 'Mantener',
    );

    if (response.confirmed) {
      await apiService.removeArticle(article: article);
    }

    notifyListeners();
    _navigationService.back();
  }

  String formatQuantity({@required ArticleModel article}) =>
      article.automaticallyNotifyShortage
          ? "${article.quantity}/${article.maximumAmountNotifyShortage}"
          : "${article.quantity}";
  lessOne({@required ArticleModel article}) async {
    if (article.quantity > 0) {
      article.quantity -= 1;

      if (article.automaticallyNotifyShortage) {
        if (article.quantity <= article.maximumAmountNotifyShortage) {
          article.shortage = true;
        } else {
          article.shortage = false;
        }
      } else {
        article.shortage = false;
      }

      var result = await apiService.updateArticle(
          article: article); // We need to add the current userId
      notifyListeners();

      if (result is String) {
        await _dialogService.showDialog(
          title: 'No se pudo incrementar el artículo.',
          description: result,
        );
      }
    }
  }

  void listenToArticles() {
    apiService.articlesRealTime(shortageArticles: shortage).listen((result) {
      List<ArticleModel> updatedArticles = result;
      if (updatedArticles != null) {
        articles = updatedArticles;
        notifyListeners();
        // print(
        //     '---------------- Articulos ${articles.length}---------------------');
        // articles.forEach((element) {
        //   print(
        //       '${element.name} - ${element.description} - ${element.quantity} - ${element.images} - ${element.automaticallyNotifyShortage}');
        // });
      }
    });
  }

  navigateToArticleEditView({ArticleModel article}) async {
    await _navigationService.navigateTo(Routes.articleEditView,
        arguments: ArticleEditViewArguments(article: article));
    if (article is ArticleModel) _navigationService.back();
  }

  navigateToArticleView({@required ArticleModel article}) async {
    await _navigationService.navigateTo(Routes.articleView,
        arguments: ArticleViewArguments(article: article));
  }

  void requestMoreData() =>
      apiService.requestMoreData(shortageArticles: shortage);
}
