import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:my_app_stacked/constant.dart';
import 'package:my_app_stacked/widgets/card_view.dart';
import 'package:stacked/stacked.dart';

import 'article_list_viewmodel.dart';

class ArticleListView extends StatelessWidget {
  const ArticleListView({bool shortage = false}) : _shortage = shortage;
  final bool _shortage;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ArticleListViewModel>.reactive(
        viewModelBuilder: () => ArticleListViewModel(shortage: _shortage),
        onModelReady: (model) => model.listenToArticles(),
        builder: (context, model, child) {
          // final double widthScreen = MediaQuery.of(context).size.width;
          // final double widthItem = widthScreen / 5;

          return Scaffold(
            appBar: AppBar(),
            body: ListView.builder(
              padding: EdgeInsets.only(left: 6, right: 6, bottom: 255),
              itemCount: model.articles.length,
              itemBuilder: (context, index) {
                var article = model.articles[index];
                var actions = [
                  Row(
                    children: [
                      Expanded(
                        child: new ListTile(
                          onTap: () => model.lessOne(article: article),
                          leading: new Icon(Icons.exposure_minus_1),
                          title: new Text('Disminuir'),
                        ),
                      ),
                      Expanded(
                        child: new ListTile(
                          onTap: () => model.addOne(article: article),
                          leading: new Icon(Icons.exposure_plus_1),
                          title: new Text('Aumentar'),
                        ),
                      ),
                    ],
                  ),
                  new ListTile(
                    onTap: () =>
                        model.navigateToArticleEditView(article: article),
                    leading: new Icon(Icons.edit),
                    title: new Text('Modificar'),
                  ),
                  new ListTile(
                    onTap: () => model.deleteArticle(article: article),
                    leading: new Icon(Icons.delete),
                    title: new Text('Eliminar'),
                  ),
                ];

                return
                    // _TileArticle(
                    //     widthItem: widthItem, article: article, actions: actions);
                    CardView(
                  onTap: () {
                    model.navigateToArticleView(article: model.articles[index]);
                  },
                  leftAction: model.articles[index].shortage
                      ? Container(
                          margin: EdgeInsets.only(left: 12),
                          child: Text('Escaso',
                              style: TextStyle(
                                  foreground: Paint()
                                    ..shader = linearGradientShader,
                                  fontWeight: FontWeight.w600)),
                        )
                      : SizedBox(),
                  quantity:
                      model.formatQuantity(article: model.articles[index]),
                  title: model.articles[index].name,
                  description: model.articles[index].description,
                  imageUrl: model.articles[index].images.isEmpty
                      ? ""
                      : model.articles[index].images.first.url,
                  bottomSheetActions: actions,
                );
              },
            ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            floatingActionButton: FloatingActionButton(
              onPressed: () {
                model.navigateToArticleEditView();
              },
              child: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          );
        });
  }
}

// class _TileArticle extends StatelessWidget {
//   const _TileArticle({
//     Key key,
//     @required this.widthItem,
//     @required this.article,
//     @required this.actions,
//   }) : super(key: key);

//   final double widthItem;
//   final ArticleModel article;
//   final List<Widget> actions;

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       margin: EdgeInsets.only(bottom: 6),
//       child: Column(
//         children: [
//           ListTile(
//             leading: SizedBox(
//               width: widthItem,
//               child: ClipRRect(
//                 borderRadius: BorderRadius.circular(6),
//                 child: Stack(
//                   children: [
//                     MyImageCache(
//                         ratio: 4 / 3,
//                         width: double.infinity,
//                         url: article.images.isNotEmpty
//                             ? article.images.first.url
//                             : ""),
//                     article.quantity == null
//                         ? SizedBox()
//                         : _QuantityIndicator(
//                             quantity: article.quantity.toString(),
//                           )
//                   ],
//                 ),
//               ),
//             ),
//             title: Text(
//               article.name,
//               maxLines: 2,
//             ),
//             subtitle: Text(
//               article.description,
//               maxLines: 2,
//               softWrap: true,
//             ),
//             onTap: () {
//               // model.navigateToArticleView(article: article);
//             },
//             trailing: GestureDetector(
//                 onTap: () {
//                   showModalBottomSheet(
//                       context: context,
//                       builder: (BuildContext bc) {
//                         return SafeArea(
//                           child: Container(
//                             decoration: BoxDecoration(
//                                 border: Border(
//                                     bottom: BorderSide(color: Colors.black12))),
//                             child: new Wrap(
//                               children: <Widget>[
//                                 Padding(
//                                   padding: EdgeInsets.symmetric(horizontal: 12),
//                                   child: Container(
//                                     decoration: BoxDecoration(
//                                         border: Border(
//                                             bottom: BorderSide(
//                                                 color: Colors.black12))),
//                                     child: actions == null
//                                         ? SizedBox()
//                                         : Wrap(children: actions),
//                                   ),
//                                 ),
//                                 FlatButton(
//                                     height: 48,
//                                     minWidth: double.infinity,
//                                     onPressed: () {
//                                       Navigator.of(context).pop();
//                                     },
//                                     child: Text(
//                                       "Cerrar",
//                                       style: TextStyle(color: headerText),
//                                     ))
//                               ],
//                             ),
//                           ),
//                         );
//                       });
//                 },
//                 child: Icon(
//                   Icons.more_vert,
//                   size: 18,
//                   color: normalText,
//                 )),
//           ),
//           Divider()
//         ],
//       ),
//     );
//   }
// }

// class _QuantityIndicator extends StatelessWidget {
//   const _QuantityIndicator({
//     Key key,
//     this.opacity = 0.6,
//     this.sigma = 1.1,
//     this.fontSize = 12.0,
//     this.quantity = "",
//   }) : super(key: key);

//   final opacity;
//   final sigma;
//   final quantity;
//   final fontSize;

//   @override
//   Widget build(BuildContext context) {
//     return Align(
//       alignment: Alignment.topLeft,
//       child: Container(
//         margin: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
//         padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
//         decoration: BoxDecoration(
//             color: Colors.black.withOpacity(opacity),
//             borderRadius: BorderRadius.circular(6)),
//         child: ClipRect(
//           child: BackdropFilter(
//             filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
//             child: Text(
//               quantity,
//               style: TextStyle(
//                   color: Colors.white,
//                   fontSize: fontSize,
//                   fontWeight: FontWeight.w500),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
