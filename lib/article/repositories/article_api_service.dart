import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:my_app_stacked/article/article.dart';

@lazySingleton
class ArticleApiService {
  static const amountPerPage = 2;

  final CollectionReference _collectionReference =
      FirebaseFirestore.instance.collection('Articles');

  final StreamController<List<ArticleModel>> _articlesController =
      StreamController<List<ArticleModel>>.broadcast();

  DocumentSnapshot _lastDocument;

  List<List<ArticleModel>> _allPagedResults = List<List<ArticleModel>>();

  bool _hasMorePosts = true;

  ArticleApiService();

  /// Add [articleModel] to firestore
  Future addArticle({@required ArticleModel article}) async {
    try {
      var result = await _collectionReference.add(article.toMap());
      print(result);
      return true;
    } catch (e) {
      return e.toString();
    }
  }

  /// Buiild the query
  Query buildQuery({bool shortageArticles = false}) {
    Query pageQuery;

    if (shortageArticles == true) {
      pageQuery = _collectionReference
          .where('shortage', isEqualTo: true)
          .orderBy('name')
          .limit(amountPerPage);
    } else {
      pageQuery = _collectionReference.orderBy('name').limit(amountPerPage);
    }

    return pageQuery;
  }

  ///get articles
  Future fetchArticlesLimit({int limit, String orderBy = "name"}) async {
    try {
      var documentSnapshot =
          await _collectionReference.orderBy(orderBy).limit(limit).get();
      if (documentSnapshot.docs.isNotEmpty) {
        return documentSnapshot.docs
            .map((snapshot) =>
                ArticleModel.fromMap(snapshot.data(), snapshot.id))
            .toList();
      } else {
        return List<ArticleModel>();
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///get articles
  Future fetchShortageArticlesLimit(
      {int limit, String orderBy = "name"}) async {
    try {
      var documentSnapshot = await _collectionReference
          .where("shortage", isEqualTo: true)
          .orderBy(orderBy)
          .limit(limit)
          .get();
      if (documentSnapshot.docs.isNotEmpty) {
        return documentSnapshot.docs
            .map((snapshot) =>
                ArticleModel.fromMap(snapshot.data(), snapshot.id))
            .toList();
      } else {
        return List<ArticleModel>();
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///get article by id
  getArticleById(String id) async {
    try {
      var doc = await _collectionReference.doc(id).get();
      return ArticleModel.fromMap(doc.data(), doc.id);
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  /// Listen articles in realtime
  Stream listenToArticlesRealTime({bool shortageArticles}) {
    _requestArticles(
      query: buildQuery(shortageArticles: shortageArticles),
    );

    return _articlesController.stream;
  }

  ///remove articles from firestore
  removeArticle({@required ArticleModel article}) async {
    await _collectionReference.doc(article.id).delete();
  }

  ///Request more data from articles in firestore
  void requestMoreData({bool shortageArticles = false}) => _requestArticles(
        query: buildQuery(shortageArticles: shortageArticles),
      );

  ///Updatye the article in firestore
  Future updateArticle({@required ArticleModel article}) async {
    try {
      await _collectionReference.doc(article.id).update(article.toMap());
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///request the article in paginate system
  void _requestArticles({
    @required Query query,
  }) {
    var pageQuery = query;

    if (_lastDocument != null) {
      pageQuery = pageQuery.startAfterDocument(_lastDocument);
    }

    if (!_hasMorePosts) {
      return;
    }

    var currentRequestIndex = _allPagedResults.length;

    pageQuery.snapshots().listen((snapshot) {
      if (snapshot.docs.isNotEmpty) {
        var articles = snapshot.docs
            .map((snapshot) =>
                ArticleModel.fromMap(snapshot.data(), snapshot.id))
            .toList();

        // Check if the page exists or not
        if (currentRequestIndex < _allPagedResults.length) {
          // If the page exists update the posts for that page
          _allPagedResults[currentRequestIndex] = articles;
        } else {
          // If the page doesn't exist add the page data
          _allPagedResults.add(articles);
        }

        // Concatenate the full list to be shown
        var allArticles = _allPagedResults.fold<List<ArticleModel>>(
            List<ArticleModel>(),
            (initialValue, pageItems) => initialValue..addAll(pageItems));

        //  Broadcase all articles

        _articlesController.add(allArticles);

        // Save the last document from the results only if it's the current last page
        if (currentRequestIndex == _allPagedResults.length - 1) {
          _lastDocument = snapshot.docs.last;
        }

        // Determine if there's more posts to request
        _hasMorePosts = articles.length == amountPerPage;
      } else {
        // _articlesController.add(List<ArticleModel>());
      }
    });
  }

  Stream articlesRealTime({
    bool shortageArticles,
  }) {
    Query pageQuery;

    if (shortageArticles == true) {
      pageQuery = _collectionReference
          .where('shortage', isEqualTo: true)
          .orderBy('name');
    } else {
      pageQuery = _collectionReference.orderBy('name');
    }

    // Register the handler for when the posts data changes
    pageQuery.snapshots().listen((postsSnapshot) {
      var posts = postsSnapshot.docs
          .map((snapshot) => ArticleModel.fromMap(snapshot.data(), snapshot.id))
          .toList();

      // Add the posts onto the controller
      _articlesController.add(posts);
    });

    // Return the stream underlying our _postsController.
    return _articlesController.stream;
  }
}
