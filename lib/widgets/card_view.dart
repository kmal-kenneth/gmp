import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:my_app_stacked/image/image.dart';

import '../constant.dart';

class CardView extends StatelessWidget {
  const CardView({
    Key key,
    this.imageUrl = "",
    this.description = "",
    this.bottomSheetActions,
    this.actions,
    this.quantity = "",
    this.leftAction,
    this.onTap,
    @required this.title,
  }) : super(key: key);

  final String imageUrl, title, description, quantity;
  final List<Widget> bottomSheetActions, actions;
  final Widget leftAction;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(6.0),
      ),
      clipBehavior: Clip.antiAlias,
      elevation: 2,
      margin: EdgeInsets.only(top: 24),
      child: InkWell(
        onTap: () => onTap(),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                MyImageCache(
                    ratio: 16 / 9, width: double.infinity, url: imageUrl),
                quantity.isEmpty
                    ? SizedBox()
                    : _QuantityIndicator(
                        quantity: quantity,
                      )
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 6,
                  ),
                  Text(
                    title,
                    maxLines: 2,
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: headerText),
                  ),
                  SizedBox(
                    height: description.isEmpty ? 0 : 6,
                  ),
                  description.isEmpty
                      ? SizedBox()
                      : Text(
                          description,
                          textAlign: TextAlign.justify,
                          maxLines: 2,
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                ],
              ),
            ),
            SizedBox(
              height: 42,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  leftAction == null ? SizedBox() : leftAction,
                  Expanded(
                    child: actions == null
                        ? SizedBox()
                        : Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [for (Widget item in actions) item],
                          ),
                  ),
                  SizedBox(
                    width: 48,
                    child: FlatButton(
                        onPressed: () {
                          showModalBottomSheet(
                              context: context,
                              builder: (BuildContext bc) {
                                return SafeArea(
                                  child: Container(
                                    decoration: BoxDecoration(
                                        border: Border(
                                            bottom: BorderSide(
                                                color: Colors.black12))),
                                    child: new Wrap(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.symmetric(
                                              horizontal: 12),
                                          child: Container(
                                            decoration: BoxDecoration(
                                                border: Border(
                                                    bottom: BorderSide(
                                                        color:
                                                            Colors.black12))),
                                            child: bottomSheetActions == null
                                                ? SizedBox()
                                                : Wrap(
                                                    children:
                                                        bottomSheetActions),
                                          ),
                                        ),
                                        FlatButton(
                                            height: 48,
                                            minWidth: double.infinity,
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "Cerrar",
                                              style:
                                                  TextStyle(color: headerText),
                                            ))
                                      ],
                                    ),
                                  ),
                                );
                              });
                        },
                        child: Icon(
                          Icons.more_vert,
                          size: 18,
                          color: normalText,
                        )),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _QuantityIndicator extends StatelessWidget {
  const _QuantityIndicator({
    Key key,
    this.opacity = 0.6,
    this.sigma = 1.1,
    this.fontSize = 12.0,
    this.quantity = "",
  }) : super(key: key);

  final opacity;
  final sigma;
  final quantity;
  final fontSize;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 6),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(opacity),
            borderRadius: BorderRadius.circular(6)),
        child: ClipRect(
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: sigma, sigmaY: sigma),
            child: Text(
              quantity,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: fontSize,
                  fontWeight: FontWeight.w500),
            ),
          ),
        ),
      ),
    );
  }
}
