import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

import 'my_switch_viewmodel.dart';

class MySwitch extends StatelessWidget {
  const MySwitch({this.value, this.onChanged});

  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<MySwitchViewmodel>.reactive(
      viewModelBuilder: () => MySwitchViewmodel(initialValue: value),
      builder: (context, model, child) => Switch(
          value: model.getValue,
          onChanged: (bool newValue) {
            model.setValue = newValue;
            this.onChanged(model.getValue);
          }),
    );
  }
}
