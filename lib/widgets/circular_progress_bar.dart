import 'package:flutter/material.dart';
import 'package:sleek_circular_slider/sleek_circular_slider.dart';

class CircularProgressBar extends StatelessWidget {
  const CircularProgressBar(
      {Key key, this.size = 32, this.progressBarWidth = 4})
      : super(key: key);

  final double size;
  final double progressBarWidth;

  @override
  Widget build(BuildContext context) {
    return SleekCircularSlider(
        appearance: CircularSliderAppearance(
      customWidths: CustomSliderWidths(progressBarWidth: progressBarWidth),
      size: size,
      customColors: CustomSliderColors(progressBarColors: [
        Color.fromRGBO(6, 196, 184, 1),
        Color.fromRGBO(42, 217, 142, 1)
      ], trackColors: [
        Color.fromRGBO(6, 196, 184, 1),
        Color.fromRGBO(42, 217, 142, 1)
      ]),
      spinnerMode: true,
    ));
  }
}
