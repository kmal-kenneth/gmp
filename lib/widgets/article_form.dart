// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:my_app_stacked/image/widgets/image_place_holder.dart';
// import 'package:my_app_stacked/ui/widgets/article_form_viewmodel.dart';
// import 'package:reorderables/reorderables.dart';
// import 'package:stacked/stacked.dart';
//
// import 'my_switch.dart';
//
// class ArticleForm extends StatelessWidget {
//   const ArticleForm(
//       {Key key, @required GlobalKey<FormState> formKey, @required this.model})
//       : _formKey = formKey,
//         super(key: key);
//
//   final GlobalKey<FormState> _formKey;
//   final ArticleFormViewmodel model;
//
//   @override
//   Widget build(BuildContext context) {
//     return ViewModelBuilder<ArticleFormViewmodel>.reactive(
//         viewModelBuilder: () => model,
//         builder: (context, model, child) {
//           return Form(
//             key: _formKey,
//             autovalidateMode: AutovalidateMode.disabled,
//             child: ListView(
//               padding: EdgeInsets.all(16),
//               children: <Widget>[
//                 SizedBox(
//                   height: 8,
//                 ),
//                 _ImagesList(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: model.images.isEmpty ? null : 8,
//                 ),
//                 Center(child: _AddImage(model: model)),
//                 SizedBox(
//                   height: 24,
//                 ),
//                 _NameInput(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: 24,
//                 ),
//                 _InputDescription(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: 24,
//                 ),
//                 _InputQuantity(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: 12,
//                 ),
//                 _InputNotifyShortage(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: 6,
//                 ),
//                 _InputAutomaticallyNotifyShortage(
//                   model: model,
//                 ),
//                 SizedBox(
//                   height: 24,
//                 ),
//                 model.article.automaticallyNotifyShortage == false
//                     ? SizedBox()
//                     : _InputMaximumAmountNotifyShortage(
//                         model: model,
//                       ),
//               ],
//             ),
//           );
//         });
//   }
// }
//
// class _ImagesList extends StatelessWidget {
//   const _ImagesList({
//     Key key,
//     @required ArticleFormViewmodel model,
//   })  : this.model = model,
//         super(key: key);
//
//   final ArticleFormViewmodel model;
//
//   @override
//   Widget build(BuildContext context) {
//     return ReorderableWrap(
//       spacing: 4.0,
//       runSpacing: 4.0,
//       maxMainAxisCount: 4,
//       children: [
//         for (final item in model.images)
//           Container(
//             key: ValueKey(item),
//             width: MediaQuery.of(context).size.width / 4 - 12,
//             child: GestureDetector(
//               onTap: () async {
//                 // model.deleteImages = await _navigationService.navigateTo(
//                 //     Routes.imageViewer,
//                 //     arguments: ImageViewerArguments(
//                 //         image: item, images: model.images));
//
//                 model.notifyListeners();
//               },
//               child: AspectRatio(
//                 aspectRatio: 4 / 3,
//                 child: item.saved
//                     ? CachedNetworkImage(
//                         fit: BoxFit.cover,
//                         imageUrl: item.image.url,
//                         placeholder: (context, url) =>
//                             ImagePlaceHolder(icon: Icons.image),
//                         errorWidget: (context, url, error) =>
//                             ImagePlaceHolder(icon: Icons.broken_image),
//                       )
//                     : Image.file(
//                         item.file,
//                         fit: BoxFit.cover,
//                       ),
//               ),
//             ),
//           ),
//       ],
//       onReorder: (oldIndex, newIndex) {
//         model.moveImage(oldIndex: oldIndex, newIndex: newIndex);
//       },
//     );
//   }
// }
//
// class _AddImage extends StatelessWidget {
//   const _AddImage({
//     Key key,
//     @required ArticleFormViewmodel model,
//   })  : this.model = model,
//         super(key: key);
//
//   final ArticleFormViewmodel model;
//
//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       // When we tap we call selectImage
//       onTap: () {
//         showModalBottomSheet(
//             context: context,
//             builder: (BuildContext bc) {
//               return SafeArea(
//                 child: Container(
//                   child: new Wrap(
//                     children: <Widget>[
//                       new ListTile(
//                           leading: new Icon(Icons.photo_library),
//                           title: new Text('Galería'),
//                           onTap: () {
//                             model.pickImage(source: ImageSource.gallery);
//                             Navigator.of(context).pop();
//                           }),
//                       new ListTile(
//                         leading: new Icon(Icons.photo_camera),
//                         title: new Text('Cámara'),
//                         onTap: () {
//                           model.pickImage(source: ImageSource.camera);
//
//                           Navigator.of(context).pop();
//                         },
//                       ),
//                     ],
//                   ),
//                 ),
//               );
//             });
//       },
//       child: SizedBox(
//         width: MediaQuery.of(context).size.width / 4 - 12,
//         child: AspectRatio(
//           aspectRatio: 4 / 3,
//           child: ImagePlaceHolder(
//             icon: Icons.add_a_photo,
//           ),
//         ),
//       ),
//     );
//   }
// }
//
// class _NameInput extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _NameInput({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       initialValue: model.article.name,
//       decoration: InputDecoration(
//           labelText: 'Nombre',
//           hintText: 'Tornillos 1 pulgada',
//           border: const OutlineInputBorder()),
//       keyboardType: TextInputType.name,
//       textCapitalization: TextCapitalization.sentences,
//       onSaved: (value) {
//         model.article.name = value.trim();
//       },
//       validator: (value) => model.validateName(value),
//     );
//   }
// }
//
// class _InputAutomaticallyNotifyShortage extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _InputAutomaticallyNotifyShortage({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         Text('Notificar escasez automáticamente'),
//         MySwitch(
//             value: model.article.automaticallyNotifyShortage,
//             onChanged: (bool newValue) {
//               model.setAutomaticallyNotifyShortage(newValue);
//             }),
//       ],
//     );
//   }
// }
//
// class _InputNotifyShortage extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _InputNotifyShortage({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Row(
//       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//       children: [
//         Text('Notificar escasez'),
//         MySwitch(
//             value: model.article.shortage,
//             onChanged: (bool newValue) {
//               model.setNotifyShortage(newValue);
//             }),
//       ],
//     );
//   }
// }
//
// class _InputDescription extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _InputDescription({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       initialValue: model.article.description,
//       decoration: InputDecoration(
//           labelText: 'Descripción',
//           hintText: 'Bolsa de 100 unidades',
//           border: const OutlineInputBorder()),
//       keyboardType: TextInputType.name,
//       textCapitalization: TextCapitalization.sentences,
//       maxLines: 3,
//       onSaved: (value) {
//         model.article.description = value;
//       },
//       validator: (value) => model.validateDescription(value),
//     );
//   }
// }
//
// class _InputMaximumAmountNotifyShortage extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _InputMaximumAmountNotifyShortage({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       initialValue: model.article.maximumAmountNotifyShortage.toString() ?? "",
//       enabled: model.article.automaticallyNotifyShortage,
//       decoration: InputDecoration(
//           labelText: 'Cantidad máxima para notificar escasez',
//           hintText: '5',
//           border: const OutlineInputBorder()),
//       keyboardType: TextInputType.number,
//       inputFormatters: <TextInputFormatter>[
//         FilteringTextInputFormatter.digitsOnly
//       ],
//       textCapitalization: TextCapitalization.sentences,
//       onSaved: (value) {
//         model.article.maximumAmountNotifyShortage = int.parse(value);
//       },
//       validator: (value) => model.validateMaximunAmountNotifyShortage(value),
//     );
//   }
// }
//
// class _InputQuantity extends StatelessWidget {
//   final ArticleFormViewmodel model;
//   const _InputQuantity({
//     Key key,
//     @required this.model,
//   }) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return TextFormField(
//       initialValue: model.article.quantity != null
//           ? model.article.quantity.toString()
//           : '',
//       decoration: InputDecoration(
//           labelText: 'Cantidad',
//           hintText: '17',
//           border: const OutlineInputBorder()),
//       keyboardType: TextInputType.number,
//       inputFormatters: <TextInputFormatter>[
//         FilteringTextInputFormatter.digitsOnly
//       ],
//       textCapitalization: TextCapitalization.sentences,
//       onSaved: (value) {
//         model.article.quantity = int.parse(value);
//       },
//       validator: (value) => model.validateQuantity(value),
//     );
//   }
// }
