// import 'package:flutter/cupertino.dart';
// import 'package:flutter/foundation.dart';
// import 'package:graphql/client.dart';
// import 'package:image_picker/image_picker.dart';
// import 'package:my_app_stacked/article/article.dart';
// import 'package:my_app_stacked/article/repositories/article_api_service.dart';
// import 'package:my_app_stacked/consonents.dart';
// import 'package:my_app_stacked/image/image.dart';
// import 'package:my_app_stacked/image/image_selector.dart';
// import 'package:my_app_stacked/locator/locator.dart';
// import 'package:stacked/stacked.dart';
// import 'package:stacked_services/stacked_services.dart';
//
// class ArticleFormViewmodel extends BaseViewModel {
//   static const String _deletePicture = '''
//   mutation deletePicture(\$id: ID!) {
//     deletePicture(input: { id: \$id }) {
//       picture {
//         objectId
//       }
//     }
//   }
//   ''';
//   static const _updateArticle = '''
//   mutation updateArticle(\$id: ID!, \$fields: UpdateArticleFieldsInput) {
//     updateArticle(input: { id: \$id, fields: \$fields }) {
//       article {
//         objectId
//       }
//     }
//   }
//   ''';
//
//   static const String _addPicture = '''
//   mutation addPicture(\$fields:CreatePictureFieldsInput!){
//     createPicture(input:{fields: \$fields} ){
//       picture{
//         name
//         objectId
//       }
//     }
//   }
//   ''';
//
//   final ArticleApiService _apiService = locator<ArticleApiService>();
//
//   final DialogService _dialogService = locator<DialogService>();
//   ArticleModel _article;
//   final ImageSelector _imageSelector = locator<ImageSelector>();
//
//   List<ImageWrapper> _images;
//
//   List<ImageWrapper> _deleteImages;
//
//   ImageWrapper selectedImage;
//
//   bool _manuallyShortage = false;
//
//   ArticleFormViewmodel({ArticleModel article}) {
//     if (article == null) {
//       _article = ArticleModel();
//     } else {
//       _article = article;
//     }
//
//     _images = List<ImageWrapper>();
//     _deleteImages = List<ImageWrapper>();
//
//     addSevedImages(images: _article.images);
//   }
//
//   ArticleModel get article => _article;
//
//   List<ImageWrapper> get images => this._images;
//
//   ///Add a new article to persistend data
//   Future addArticle() async {
//     var result = await _apiService.addArticle(
//         article: _article); // We need to add the current userId
//
//     if (result is String) {
//       await _dialogService.showDialog(
//         title: 'No se pudo agregar el artículo.',
//         description: result,
//       );
//     }
//   }
//
//   ///Add saved images to [images] for show in view
//   void addSevedImages({@required List<ImageModel> images}) {
//     if (_article.images != null && _article.images.isNotEmpty) {
//       _article.images.forEach((image) {
//         this.images.add(ImageWrapper(saved: true, image: image));
//       });
//
//       notifyListeners();
//     }
//   }
//
//   /// Delete saved images of persistend data
//   deleteSavedImages() async {
//     for (var item in _deleteImages) {
//       final MutationOptions options = MutationOptions(
//         documentNode: gql(_deletePicture),
//         variables: {"id": item.image.objectId},
//       );
//
//       final QueryResult result = await client.mutate(options);
//
//       if (result.hasException) {
//         print(result.exception.toString());
//       }
//     }
//   }
//
//   ///Move image in the [images]
//   void moveImage({@required int oldIndex, @required int newIndex}) {
//     final ImageWrapper temp = _images.removeAt(oldIndex);
//     _images.insert(newIndex, temp);
//
//     notifyListeners();
//   }
//
//   /// Get image form galery or camera
//   Future pickImage({@required ImageSource source}) async {
//     var tempImage = await _imageSelector.selectImage(source: source);
//     if (tempImage != null) {
//       var image = ImageWrapper(
//         saved: false,
//         file: tempImage,
//       );
//
//       _images.add(image);
//
//       notifyListeners();
//     }
//   }
//
//   /// Save the actual article
//   ///
//   /// If [tempArticle.id.isNotEmpty] update the article.
//   /// But if [tempArticle.id] is empty add the article.
//   save() async {
//     setBusy(true);
//     notifyListeners();
//
//     _article.images.clear();
//
//     for (var imageWrap in _images) {
//       if (imageWrap.image.objectId == null) {
//         _article.images.add(await uploadImage(imageWrap: imageWrap));
//       } else {
//         _article.images.add(imageWrap.image);
//       }
//     }
//
//     if (_article.automaticallyNotifyShortage) {
//       if (_article.quantity <= _article.maximumAmountNotifyShortage) {
//         _article.shortage = true;
//       } else {
//         _article.shortage = false;
//       }
//     }
//
//     if (_manuallyShortage) {
//       _article.shortage = true;
//     }
//
//     if (_article.id.isNotEmpty) {
//       if (_deleteImages.isNotEmpty) {
//         await deleteSavedImages();
//       }
//
//       await updateArticle();
//     } else {
//       await addArticle();
//     }
//
//     setBusy(false);
//     notifyListeners();
//   }
//
//   ///Set [article.automaticallyNotifyShortage] for show other part of the view
//   setAutomaticallyNotifyShortage(value) {
//     _article.automaticallyNotifyShortage = value;
//     notifyListeners();
//   }
//
//   void setNotifyShortage(bool newValue) {
//     _manuallyShortage = newValue;
//     notifyListeners();
//   }
//
//   ///Update the saved article in the persistend data
//   Future<void> updateArticle() async {
//     final MutationOptions options = MutationOptions(
//         documentNode: gql(_updateArticle), variables: _article.toGraphql());
//
//     final QueryResult result = await client.mutate(options);
//
//     print(result.data);
//
//     if (result.hasException) {
//       await _dialogService.showDialog(
//         title: 'No se pudo modificar el artículo.',
//         description: result.exception.toString(),
//       );
//     }
//   }
//
//   ///Upload no saved images to firebase
//   Future<ImageModel> uploadImage({ImageWrapper imageWrap}) async {
//     var imageFileName =
//         _article.name + DateTime.now().millisecondsSinceEpoch.toString();
//
//     final MutationOptions options = MutationOptions(
//       documentNode: gql(_addPicture),
//       variables: {
//         "fields": {
//           "name": imageFileName,
//           "image": {"file": imageWrap.file}
//         }
//       },
//     );
//
//     final QueryResult result = await client.mutate(options);
//
//     if (result.hasException) {
//       print(result.exception.toString());
//     }
//
//     return ImageModel(objectId: result.data["objectId"]);
//   }
//
//   validateDescription(value) {
//     var tempValue = value.trim();
//     if (tempValue.isEmpty) {
//       return 'Se requiere una descripción.';
//     }
//     return null;
//   }
//
//   validateMaximunAmountNotifyShortage(value) {
//     var tempValue = int.tryParse(value.trim());
//
//     if (value.isEmpty) {
//       return 'Se requiere una cantidad.';
//     }
//
//     if (tempValue == null) {
//       return 'Debe ser un número.';
//     } else if (tempValue <= 0) {
//       return 'Debe ser mayor a 0.';
//     }
//
//     return null;
//   }
//
//   validateName(value) {
//     var tempValue = value.trim();
//     if (tempValue.isEmpty) {
//       return 'Se requiere un nombre.';
//     }
//     return null;
//   }
//
//   validateQuantity(value) {
//     var tempValue = value.trim();
//
//     if (tempValue.isEmpty) {
//       return 'Se requiere una cantidad.';
//     }
//
//     if (int.tryParse(value) == null) {
//       return 'Debe ser un número.';
//     }
//     return null;
//   }
// }
