import 'package:stacked/stacked.dart';

class MySwitchViewmodel extends BaseViewModel {
  MySwitchViewmodel({bool initialValue}) {
    if (initialValue != null) {
      this._value = initialValue;
      notifyListeners();
    }
  }

  bool _value = false;

  get getValue => this._value;

  set setValue(bool value) {
    this._value = value;
    notifyListeners();
  }
}
