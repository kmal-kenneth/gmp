import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:my_app_stacked/router/router.gr.dart' as router;
import 'package:my_app_stacked/widgets/circular_progress_bar.dart';
import 'package:oktoast/oktoast.dart';
import 'package:round_indicator/round_indicator.dart';
import 'package:stacked_services/stacked_services.dart';

import 'constant.dart';
import 'locator/locator.dart';

void main() {
  // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
  //   statusBarColor: Colors.white, // status bar color
  //   statusBarBrightness: Brightness.light, //status bar brigtness
  //   statusBarIconBrightness: Brightness.light, //status barIcon Brightness
  //   systemNavigationBarColor: Colors.white, // navigation bar color
  //   systemNavigationBarDividerColor: normalText, //Navigation bar divider color
  //   systemNavigationBarIconBrightness: Brightness.dark, //navigation bar icon
  // ));

  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text('Error in firebase');
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          // SystemChrome.setEnabledSystemUIOverlays([]);

          return OKToast(
            child: MaterialApp(
              theme: ThemeData(
                  splashColor: segundary,
                  scaffoldBackgroundColor: Colors.white,
                  primaryColor: prymary,
                  accentColor: segundary,
                  cardTheme: CardTheme(shadowColor: Colors.black45),
                  iconTheme: IconThemeData(color: normalText),
                  backgroundColor: Colors.white,
                  bottomSheetTheme: BottomSheetThemeData(
                    shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(8)),
                    ),
                  ),
                  textTheme: TextTheme(
                    bodyText1: TextStyle(color: headerText),
                    bodyText2: TextStyle(color: normalText),
                    headline1: TextStyle(color: Colors.red),
                    headline2: TextStyle(color: Colors.red),
                    headline3: TextStyle(color: Colors.red),
                    headline4: TextStyle(color: Colors.red),
                    headline5: TextStyle(color: Colors.red),
                    headline6: TextStyle(color: Colors.red),
                    overline: TextStyle(color: headerText),
                    subtitle1: TextStyle(color: headerText), //listile title
                    subtitle2: TextStyle(color: normalText),
                  ),
                  appBarTheme: AppBarTheme(
                      elevation: 0,
                      color: Colors.white,
                      textTheme: TextTheme(
                          headline6: TextStyle(
                              color: normalText,
                              fontWeight: FontWeight.w500,
                              fontSize: 16)),
                      iconTheme: IconThemeData(color: normalText),
                      actionsIconTheme: IconThemeData(color: normalText)),
                  primaryIconTheme: IconThemeData(color: normalText),
                  tabBarTheme: TabBarTheme(
                      // indicatorSize: TabBarIndicatorSize.label,
                      indicator: RoundTabIndicator(
                        borderSide: BorderSide(width: 4),
                        gradient: linearGradient,
                      ),
                      labelStyle: TextStyle(
                          fontSize: 18,
                          foreground: Paint()..shader = linearGradientShader,
                          fontWeight: FontWeight.w600),
                      unselectedLabelStyle: TextStyle(
                          color: normalText, fontWeight: FontWeight.w500))),
              debugShowCheckedModeBanner: false,
              title: 'GMP',
              initialRoute: router.Routes.startupView,
              onGenerateRoute: router.Router().onGenerateRoute,
              navigatorKey: locator<NavigationService>().navigatorKey,
            ),
          );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return Container(
            color: Colors.white, child: Center(child: CircularProgressBar()));
      },
    );
  }
}
