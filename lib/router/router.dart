import 'package:auto_route/auto_route_annotations.dart';
import 'package:my_app_stacked/article/views/article_edit_view.dart';
import 'package:my_app_stacked/article/views/article_list_view.dart';
import 'package:my_app_stacked/article/views/article_view.dart';
import 'package:my_app_stacked/image/views/image_viewer.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/provider/views/provider_list_view.dart';
import 'package:my_app_stacked/provider/views/provider_view.dart';
import 'package:my_app_stacked/ui/views/startup/startup_view.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: StartupView, initial: true),
  MaterialRoute(page: ArticleView),
  MaterialRoute(page: ArticleEditView),
  MaterialRoute(page: ArticleListView),
  MaterialRoute(page: ImageViewer),
  MaterialRoute(page: ProviderListView),
  MaterialRoute(page: ProviderEditView),
  MaterialRoute(page: ProviderView),
])
class $Router {}
