// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../article/article.dart';
import '../article/views/article_edit_view.dart';
import '../article/views/article_list_view.dart';
import '../article/views/article_view.dart';
import '../image/image.dart';
import '../image/views/image_viewer.dart';
import '../provider/provider.dart';
import '../provider/views/provider_list_view.dart';
import '../provider/views/provider_view.dart';
import '../ui/views/startup/startup_view.dart';

class Routes {
  static const String startupView = '/';
  static const String articleView = '/article-view';
  static const String articleEditView = '/article-edit-view';
  static const String articleListView = '/article-list-view';
  static const String imageViewer = '/image-viewer';
  static const String providerListView = '/provider-list-view';
  static const String providerEditView = '/provider-edit-view';
  static const String providerView = '/provider-view';
  static const all = <String>{
    startupView,
    articleView,
    articleEditView,
    articleListView,
    imageViewer,
    providerListView,
    providerEditView,
    providerView,
  };
}

class Router extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startupView, page: StartupView),
    RouteDef(Routes.articleView, page: ArticleView),
    RouteDef(Routes.articleEditView, page: ArticleEditView),
    RouteDef(Routes.articleListView, page: ArticleListView),
    RouteDef(Routes.imageViewer, page: ImageViewer),
    RouteDef(Routes.providerListView, page: ProviderListView),
    RouteDef(Routes.providerEditView, page: ProviderEditView),
    RouteDef(Routes.providerView, page: ProviderView),
  ];
  @override
  Map<Type, AutoRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, AutoRouteFactory>{
    StartupView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => StartupView(),
        settings: data,
      );
    },
    ArticleView: (data) {
      final args = data.getArgs<ArticleViewArguments>(
        orElse: () => ArticleViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ArticleView(article: args.article),
        settings: data,
      );
    },
    ArticleEditView: (data) {
      final args = data.getArgs<ArticleEditViewArguments>(
        orElse: () => ArticleEditViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ArticleEditView(article: args.article),
        settings: data,
      );
    },
    ArticleListView: (data) {
      final args = data.getArgs<ArticleListViewArguments>(
        orElse: () => ArticleListViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ArticleListView(shortage: args.shortage),
        settings: data,
      );
    },
    ImageViewer: (data) {
      final args = data.getArgs<ImageViewerArguments>(
        orElse: () => ImageViewerArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ImageViewer(
          key: args.key,
          image: args.image,
          images: args.images,
          deleteAction: args.deleteAction,
        ),
        settings: data,
      );
    },
    ProviderListView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => ProviderListView(),
        settings: data,
      );
    },
    ProviderEditView: (data) {
      final args = data.getArgs<ProviderEditViewArguments>(
        orElse: () => ProviderEditViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ProviderEditView(provider: args.provider),
        settings: data,
      );
    },
    ProviderView: (data) {
      final args = data.getArgs<ProviderViewArguments>(
        orElse: () => ProviderViewArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => ProviderView(provider: args.provider),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// ArticleView arguments holder class
class ArticleViewArguments {
  final ArticleModel article;
  ArticleViewArguments({this.article});
}

/// ArticleEditView arguments holder class
class ArticleEditViewArguments {
  final ArticleModel article;
  ArticleEditViewArguments({this.article});
}

/// ArticleListView arguments holder class
class ArticleListViewArguments {
  final bool shortage;
  ArticleListViewArguments({this.shortage = false});
}

/// ImageViewer arguments holder class
class ImageViewerArguments {
  final Key key;
  final ImageWrapper image;
  final List<ImageWrapper> images;
  final bool deleteAction;
  ImageViewerArguments(
      {this.key, this.image, this.images, this.deleteAction = false});
}

/// ProviderEditView arguments holder class
class ProviderEditViewArguments {
  final ProviderModel provider;
  ProviderEditViewArguments({this.provider});
}

/// ProviderView arguments holder class
class ProviderViewArguments {
  final ProviderModel provider;
  ProviderViewArguments({this.provider});
}
