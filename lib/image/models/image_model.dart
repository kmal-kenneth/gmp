import 'dart:convert';

ImageModel imageFromMap(String str) => ImageModel.fromMap(json.decode(str));

String imageToMap(ImageModel data) => json.encode(data.toMap());

class ImageModel {
  String url;
  String name;
  ImageModel({
    this.url,
    this.name,
  });

  factory ImageModel.fromMap(Map<String, dynamic> json) => ImageModel(
        name: json["name"],
        url: json["url"],
      );

  Map<String, dynamic> toMap() => {
        "name": name,
        "url": url,
      };
}
