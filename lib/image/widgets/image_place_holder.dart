import 'package:flutter/material.dart';

import '../../constant.dart';

class ImagePlaceHolder extends StatelessWidget {
  final IconData icon;
  final double size;
  const ImagePlaceHolder({Key key, @required this.icon, this.size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: linearGradient
      ),
      child: Center(
        child: Icon(
          icon,
          color: Colors.white,
          size: size,
        ),
      ),
    );
  }
}
