import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_app_stacked/image/widgets/image_place_holder.dart';

class MyImageCache extends StatelessWidget {
  final String url;
  final double width, height, ratio;

  const MyImageCache(
      {Key key, this.url = "", this.width, this.height, this.ratio = 4 / 3})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      height: this.height,
      child: AspectRatio(
        aspectRatio: this.ratio,
        child: url.isNotEmpty
            ? CachedNetworkImage(
                width: double.infinity,
                height: double.infinity,
                fit: BoxFit.cover,
                imageUrl: url,
                placeholder: (context, url) =>
                    ImagePlaceHolder(icon: Icons.image),
                errorWidget: (context, url, error) =>
                    ImagePlaceHolder(icon: Icons.broken_image),
              )
            : ImagePlaceHolder(icon: Icons.image_not_supported),
      ),
    );
  }
}
