import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ImageViewerViewModel extends BaseViewModel {
  ImageWrapper image;
  bool hideUI = true;
  final NavigationService _navigationService = locator<NavigationService>();
  List<ImageWrapper> images;
  List<ImageWrapper> deleteImages;

  ImageViewerViewModel({this.image, this.images}) {
    deleteImages = List<ImageWrapper>();
  }

  ///If no [image.saved] are discard in other case is added to [deleteImages]
  addToDeleteList() {
    int index = images.indexOf(image);

    if (image.saved) {
      deleteImages.add(image);
    }

    images.removeAt(index);

    if (images.isEmpty) {
      closeView();
    } else if (index < images.length - 1) {
      image = images[index];
    } else {
      image = images.last;
    }

    notifyListeners();
  }

  /// Return to previus view and return [deleteImages]
  closeView() {
    _navigationService.back(result: deleteImages);
  }

  ///Change the selected [image]
  void selectImageGesture({ImageWrapper image}) {
    this.image = image;
    notifyListeners();
  }

  ///Show and hide the ui
  switchUI() {
    // if (hideUI) {
    //   SystemChrome.setEnabledSystemUIOverlays([]);
    // } else {
    //   SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    // }

    hideUI = !hideUI;

    notifyListeners();
  }
}
