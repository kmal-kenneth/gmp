import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:my_app_stacked/constant.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/image/widgets/image_place_holder.dart';
import 'package:stacked/stacked.dart';

import 'image_viewer_viewmodel.dart';

class ImageViewer extends StatefulWidget {
  final ImageViewerViewModel _model = ImageViewerViewModel();

  final bool deleteAction;

  ImageViewer(
      {Key key,
      ImageWrapper image,
      List<ImageWrapper> images,
      this.deleteAction = false})
      : super(key: key) {
    _model.image = image;
    _model.images = images;
  }

  @override
  _ImageViewerState createState() =>
      _ImageViewerState(model: _model, deleteAction: deleteAction);
}

class _ImageViewerState extends State<ImageViewer> {
  ImageViewerViewModel _model;
  final bool deleteAction;

  _ImageViewerState({ImageViewerViewModel model, this.deleteAction}) {
    _model = model;
  }

  @override
  Widget build(BuildContext context) {
    final TransformationController transformationController =
        TransformationController();

    return ViewModelBuilder<ImageViewerViewModel>.reactive(
      viewModelBuilder: () => _model,
      builder: (context, model, child) {
        return Scaffold(
          extendBodyBehindAppBar: true,
          backgroundColor: Colors.black26,
          appBar: !model.hideUI
              ? null
              : PreferredSize(
                  preferredSize: Size(double.infinity, 48),
                  child: FadeIn(
                    animate: model.hideUI,
                    child: FadeOut(
                      animate: model.hideUI == false,
                      child: Container(
                        width: double.infinity,
                        padding: EdgeInsets.symmetric(vertical: 6),
                        decoration:
                            BoxDecoration(color: Colors.black.withOpacity(.6)),
                        child: ClipRect(
                          child: BackdropFilter(
                            filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                            child: AppBar(
                              backgroundColor: Colors.transparent,
                              brightness: Brightness.dark,
                              elevation: 0,
                              leading: BackButton(
                                onPressed: () {
                                  model.closeView();
                                },
                              ),
                              actions: [
                                deleteAction == true
                                    ? FlatButton(
                                        onPressed: () async {
                                          model.addToDeleteList();
                                        },
                                        child: Icon(
                                          Icons.delete,
                                          color: normalText,
                                        )
                                        // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                                        )
                                    : SizedBox()
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
          body: GestureDetector(
            onTap: () {
              model.switchUI();
            },
            child: Stack(children: [
              SizedBox(
                height: double.infinity,
                width: double.infinity,
                child: InteractiveViewer(
                  transformationController:
                      transformationController, // pass the transformation controller
                  minScale: 0.1, // min scale
                  maxScale: 4.6, // max scale
                  scaleEnabled: true,
                  panEnabled: true,
                  child: model.image != null
                      ? model.image.saved
                          ? CachedNetworkImage(
                              fit: BoxFit.contain,
                              imageUrl: model.image.image.url,
                              placeholder: (context, url) =>
                                  ImagePlaceHolder(icon: Icons.image),
                              errorWidget: (context, url, error) =>
                                  ImagePlaceHolder(icon: Icons.broken_image),
                            )
                          : Image.file(
                              model.image.file,
                              fit: BoxFit.contain,
                            )
                      : ImagePlaceHolder(icon: Icons.image),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: !model.hideUI
                    ? null
                    : FadeIn(
                        animate: model.hideUI,
                        child: FadeOut(
                          animate: model.hideUI == false,
                          child: Container(
                            height: 64,
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(vertical: 6),
                            decoration: BoxDecoration(
                                color: Colors.black.withOpacity(.6)),
                            child: ClipRect(
                              child: BackdropFilter(
                                  filter:
                                      ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                                  child: ListView(
                                    scrollDirection: Axis.horizontal,
                                    children: [
                                      for (final item in model.images)
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                              horizontal: 6),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            child: InkWell(
                                              key: ValueKey(item),
                                              onTap: () async {
                                                model.selectImageGesture(
                                                    image: item);
                                                setState(() {
                                                  transformationController
                                                      .toScene(Offset.zero);
                                                });
                                              },
                                              child: AspectRatio(
                                                aspectRatio: 4 / 3,
                                                child: item.saved
                                                    ? CachedNetworkImage(
                                                        fit: BoxFit.cover,
                                                        imageUrl:
                                                            item.image.url,
                                                        placeholder: (context,
                                                                url) =>
                                                            ImagePlaceHolder(
                                                                icon: Icons
                                                                    .image),
                                                        errorWidget: (context,
                                                                url, error) =>
                                                            ImagePlaceHolder(
                                                                icon: Icons
                                                                    .broken_image),
                                                      )
                                                    : Image.file(
                                                        item.file,
                                                        fit: BoxFit.cover,
                                                      ),
                                              ),
                                            ),
                                          ),
                                        ),
                                    ],
                                  )),
                            ),
                          ),
                        ),
                      ),
              )
            ]),
          ),
        );
      },
    );
  }
}
