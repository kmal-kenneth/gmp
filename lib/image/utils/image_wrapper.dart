import 'dart:io';

import 'package:my_app_stacked/image/image.dart';

class ImageWrapper {
  ImageModel image;
  File file;
  bool saved = false;

  ImageWrapper({this.image, this.file, this.saved});
}
