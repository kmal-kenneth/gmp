import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
class ImageSelector {
  ImagePicker picker;

  ImageSelector() {
    picker = ImagePicker();
  }

  Future<File> selectImage({@required ImageSource source}) async {
    PickedFile pickedFile;

    switch (source) {
      case ImageSource.camera:
        pickedFile = await _imgFromCamera();
        break;
      case ImageSource.gallery:
        pickedFile = await _imgFromGallery();
        break;
    }

    if (pickedFile != null) {
      return File(pickedFile.path);
    }

    return null;
  }

  Future<PickedFile> _imgFromCamera() async {
    return await picker.getImage(source: ImageSource.camera);
  }

  Future<PickedFile> _imgFromGallery() async {
    return await picker.getImage(source: ImageSource.gallery);
  }
}
