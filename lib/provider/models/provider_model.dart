// To parse this JSON data, do
//
//     final provider = providerFromMap(jsonString);

import 'dart:convert';

import 'package:my_app_stacked/image/image.dart';

ProviderModel providerFromMap(String str, id) =>
    ProviderModel.fromMap(json.decode(str), id);

String providerToMap(ProviderModel data) => json.encode(data.toMap());

class ProviderModel {
  String id;

  String description;
  String name;
  String telephone;
  String email;
  String address;
  List<ImageModel> images;
  ProviderModel(
      {this.id,
      this.description,
      this.name,
      this.images,
      this.address,
      this.email,
      this.telephone}) {
    images = List<ImageModel>();
  }

  factory ProviderModel.fromMap(Map<String, dynamic> json, String id) {
    var list = json['images'] as List;

    var provider = ProviderModel(
      id: id ?? '',
      description: json["description"],
      name: json["name"],
      telephone: json["telephone"],
      email: json["email"],
      address: json["address"],
    );

    provider.images = list.map((i) => ImageModel.fromMap(i)).toList();
    return provider;
  }

  Map<String, dynamic> toMap() => {
        "description": description,
        "name": name,
        "telephone": telephone,
        "email": email,
        "address": address,
        "images": images.map((image) => image.toMap()).toList()
      };
}
