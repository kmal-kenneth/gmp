import 'dart:convert';

ProductModel productFromMap(String str, id) =>
    ProductModel.fromMap(json.decode(str), id);

String productToMap(ProductModel data) => json.encode(data.toMap());

class ProductModel {
  String id;

  String articleId;
  String providerId;
  double price;
  ProductModel({this.articleId, this.providerId, this.price, this.id});

  factory ProductModel.fromMap(Map<String, dynamic> json, String id) =>
      ProductModel(
        id: id ?? '',
        articleId: json["articleId"],
        providerId: json["providerId"],
        price: json["price"],
      );

  Map<String, dynamic> toMap() => {
        "articleId": articleId,
        "providerId": providerId,
        "price": price,
      };
}
