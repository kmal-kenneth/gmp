import 'package:flutter/material.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/provider/views/provider_view_viewmodel.dart';
import 'package:my_app_stacked/image/widgets/my_image_cache.dart';
import 'package:stacked/stacked.dart';

import '../../constant.dart';

class ProviderView extends StatelessWidget {
  final ProviderModel _provider;
  const ProviderView({ProviderModel provider}) : _provider = provider;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProviderViewViewmodel>.reactive(
      viewModelBuilder: () => ProviderViewViewmodel(provider: _provider),
      builder: (context, model, child) {
        final int numberOfProviders = 3;
        final int padding = 14;

        final double widthScreen = MediaQuery.of(context).size.width;
        final double widthItem = widthScreen / numberOfProviders - padding;
        final double heigthItem = widthItem * 3 / 4;
        // final double widthItemCategory = widthScreen / numberOfProviders - padding;
        // final double heigthItemCategoy = widthItemCategory * 4 / 3;

        return Scaffold(
          appBar: AppBar(
            leading: BackButton(onPressed: () => model.navigationBack()),
          ),
          body: Container(
            color: Colors.white,
            child: ListView(
              children: [
                MyImageCache(
                  url: model.urlFirstImage,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        model.provider.name,
                        style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: headerText),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 24),
                        child: Text(
                          model.provider.description,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: normalText),
                        ),
                      ),
                      Divider(
                        thickness: 1.2,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      ListTile(
                        leading: Icon(Icons.phone),
                        title: Text(model.provider.telephone),
                      ),
                      ListTile(
                        leading: Icon(Icons.mail),
                        title: Text(model.provider.email),
                      ),
                      ListTile(
                        leading: Icon(Icons.place),
                        title: Text(model.provider.address),
                      ),
                      Divider(
                        thickness: 1.2,
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Text(
                        'Galería',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w800,
                            color: headerText),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      SizedBox(
                        height: heigthItem,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            for (var item in model.images)
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 3),
                                child: InkWell(
                                  onTap: () {
                                    model.navigationtoImageViewer(
                                        select: item, images: model.images);
                                  },
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(6),
                                    child: MyImageCache(
                                      url: item.image.url,
                                    ),
                                  ),
                                ),
                              )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
