import 'package:email_validator/email_validator.dart';
import 'package:flutter/foundation.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/image/image_selector.dart';
import 'package:my_app_stacked/image/repository/image_repository.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProviderEditViewModel extends BaseViewModel {
  final ProviderRepositoryService _repositoryService =
      locator<ProviderRepositoryService>();
  final DialogService _dialogService = locator<DialogService>();

  ProviderModel provider;
  ProviderModel providerTemp;

  // String title;

  final ImageSelector _imageSelector = locator<ImageSelector>();

  final ImageRepository _cloudStorageService = locator<ImageRepository>();
  List<ImageWrapper> images;
  List<ImageWrapper> deleteImages;
  ImageWrapper selectedImage;

  ProviderEditViewModel({this.provider}) {
    if (provider == null) {
      provider = ProviderModel();
      // title = 'Crear proveedor';
    } else {
      // title = 'Modificar proveedor';
    }
    providerTemp = ProviderModel.fromMap(provider.toMap(), provider.id);

    images = List<ImageWrapper>();
    deleteImages = List<ImageWrapper>();

    addSevedImages(images: providerTemp.images);
  }

  ///Add a new provider to persistend data
  Future addProvider() async {
    var result = await _repositoryService.addProvider(
        provider: provider); // We need to add the current userId

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo agregar el proveedor.',
        description: result,
      );
    }
  }

  ///Add saved images to [images] for show in view
  void addSevedImages({@required List<ImageModel> images}) {
    if (providerTemp.images != null && providerTemp.images.isNotEmpty) {
      providerTemp.images.forEach((image) {
        this.images.add(ImageWrapper(saved: true, image: image));
      });

      notifyListeners();
    }
  }

  /// Delete saved images of persistend data
  deleteSavedImages() {
    deleteImages.forEach((imageWrap) async {
      await _cloudStorageService.deleteImage(imageWrap.image.name);
      provider.images.remove(imageWrap.image);
    });
  }

  ///Move image in the [images]
  void moveImage({@required int oldIndex, @required int newIndex}) {
    final ImageWrapper temp = images.removeAt(oldIndex);
    images.insert(newIndex, temp);

    notifyListeners();
  }

  /// Get image form galery or camera
  Future pickImage({@required ImageSource source}) async {
    var tempImage = await _imageSelector.selectImage(source: source);
    if (tempImage != null) {
      var image = ImageWrapper(
        saved: false,
        file: tempImage,
      );

      images.add(image);

      notifyListeners();
    }
  }

  /// Save the actual provider
  ///
  /// If [tempProvider.id.isNotEmpty] update the provider.
  /// But if [tempProvider.id] is empty add the provider.
  save() async {
    setBusy(true);
    notifyListeners();
    provider = providerTemp;

    provider.images.clear();

    for (var imageWrap in images) {
      if (imageWrap.saved == false) {
        provider.images.add(await uploadImage(imageWrap: imageWrap));
      } else {
        provider.images.add(imageWrap.image);
      }
    }

    if (provider.id.isNotEmpty) {
      if (deleteImages.isNotEmpty) {
        await deleteSavedImages();
      }

      await updateProvider();
    } else {
      await addProvider();
    }

    setBusy(false);
    notifyListeners();
  }

  ///Update the saved provider in the persistend data
  Future<void> updateProvider() async {
    // We need to add the current userId
    var result = await _repositoryService.updateProvider(provider: provider);

    if (result is String) {
      await _dialogService.showDialog(
        title: 'No se pudo modificar el proveedor.',
        description: result,
      );
    }
  }

  ///Upload no saved images to firebase
  Future<ImageModel> uploadImage({ImageWrapper imageWrap}) async {
    CloudStorageResult storageResult;
    storageResult = await _cloudStorageService.uploadImage(
        imageToUpload: imageWrap.file, title: provider.name);
    return ImageModel(
        name: storageResult.imageFileName, url: storageResult.imageUrl);
  }

  validateDescription(value) {
    var tempValue = value.trim();
    if (tempValue.isEmpty) {
      return 'Se requiere una descripción.';
    }
    return null;
  }

  validateMaximunAmountNotifyShortage(value) {
    var tempValue = int.tryParse(value.trim());

    if (value.isEmpty) {
      return 'Se requiere una cantidad.';
    }

    if (tempValue == null) {
      return 'Debe ser un número.';
    } else if (tempValue <= 0) {
      return 'Debe ser mayor a 0.';
    }

    return null;
  }

  validateName(value) {
    var tempValue = value.trim();
    if (tempValue.isEmpty) {
      return 'Se requiere un nombre.';
    }
    return null;
  }

  validateQuantity(value) {
    var tempValue = value.trim();

    if (tempValue.isEmpty) {
      return 'Se requiere una cantidad.';
    }

    if (int.tryParse(value) == null) {
      return 'Debe ser un número.';
    }
    return null;
  }

  validateEmail(String value) {
    var tempValue = value.trim();
    if (tempValue.isNotEmpty) {
      if (EmailValidator.validate(tempValue) == false) {
        return 'Debe ser una dirección de email válida.';
      }
    }
    return null;
  }

  validatePhone(String value) {
    var tempValue = value.trim();

    if (tempValue.isEmpty) {
      return null;
    }

    // You may need to change this pattern to fit your requirement.
    // I just copied the pattern from here: https://regexr.com/3c53v
    const pattern = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    final regExp = RegExp(pattern);

    if (!regExp.hasMatch(tempValue)) {
      return 'Debe ser un número de teléfono válido.';
    }

    return null;
  }
}
