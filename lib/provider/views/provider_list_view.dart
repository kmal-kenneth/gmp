import 'package:flutter/material.dart';
import 'package:my_app_stacked/provider/views/provider_list_viewmodel.dart';
import 'package:my_app_stacked/widgets/card_view.dart';
import 'package:stacked/stacked.dart';

class ProviderListView extends StatelessWidget {
  const ProviderListView();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProviderListViewmodel>.reactive(
      viewModelBuilder: () => ProviderListViewmodel(),
      onModelReady: (model) => model.listenToProviders(),
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(),
        body: model.providers.isEmpty
            ? Center(
                child: Text("No hay proveedores en el sistema."),
              )
            : ListView.builder(
                padding: EdgeInsets.fromLTRB(12, 0, 12, 12),
                itemCount: model.providers.length,
                itemBuilder: (context, index) => CardView(
                      onTap: () {
                        model.navigateToProviderView(
                            provider: model.providers[index]);
                      },
                      title: model.providers[index].name,
                      description: model.providers[index].description,
                      imageUrl: model.providers[index].images.isEmpty
                          ? ""
                          : model.providers[index].images.first.url,
                      bottomSheetActions: <Widget>[
                        InkWell(
                          onTap: () => model.navigateToProviderEditView(
                              provider: model.providers[index]),
                          child: new ListTile(
                            leading: new Icon(Icons.edit),
                            title: new Text('Modificar'),
                          ),
                        ),
                        InkWell(
                          onTap: () => model.deleteProvider(
                              provider: model.providers[index]),
                          child: new ListTile(
                            leading: new Icon(Icons.delete),
                            title: new Text('Eliminar'),
                          ),
                        ),
                      ],
                    )),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            model.navigateToProviderEditView();
          },
          child: Icon(
            Icons.add,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}
