import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_app_stacked/constant.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/provider/views/provider_edit_viewmodel.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:my_app_stacked/widgets/circular_progress_bar.dart';
import 'package:oktoast/oktoast.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import 'package:reorderables/reorderables.dart';

class ProviderEditView extends StatelessWidget {
  final ProviderModel _provider;
  const ProviderEditView({ProviderModel provider}) : _provider = provider;

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<ProviderEditViewModel>.reactive(
      viewModelBuilder: () => ProviderEditViewModel(provider: _provider),
      builder: (context, model, child) => MyForm(model: model),
    );
  }
}

class MyForm extends StatefulWidget {
  final ProviderEditViewModel _model;

  const MyForm({
    Key key,
    @required ProviderEditViewModel model,
  })  : _model = model,
        super(key: key);

  @override
  _MyFormState createState() => _MyFormState();
}

class SaveButton extends StatelessWidget {
  final GlobalKey<FormState> _formKey;

  final ProviderEditViewModel model;
  const SaveButton({
    Key key,
    @required GlobalKey<FormState> formKey,
    @required this.model,
  })  : _formKey = formKey,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: model.isBusy == true
          ? null
          : () async {
              final form = _formKey.currentState;

              if (form.validate()) {
                form.save();

                await model.save();

                showToast(
                  model.providerTemp.id.isEmpty
                      ? "Se agrego exitosamente"
                      : "Se guardo exitosamente",
                  textStyle: TextStyle(fontSize: 16.0, color: Colors.white),
                  textPadding:
                      EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                  position: ToastPosition.bottom,
                  backgroundColor: Colors.black.withOpacity(0.55),
                  radius: 100.0,
                );

                final NavigationService _navigationService =
                    locator<NavigationService>();

                _navigationService.back(result: true);
              }
            },
      child: model.isBusy == false
          ? Text(
              'Guardar',
              style: TextStyle(color: normalText),
            )
          : CircularProgressBar(
              size: 20,
            ),

      // shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
    );
  }
}

class _InputDescription extends StatelessWidget {
  final ProviderEditViewModel model;
  const _InputDescription({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      textInputAction: TextInputAction.newline,
      initialValue: model.providerTemp.description,
      decoration: InputDecoration(
        labelText: 'Descripción',
        hintText: 'Vidrios & Herrajes Costa Rica',
      ),
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      maxLines: null,
      onSaved: (value) {
        model.providerTemp.description = value.trim();
      },
      validator: (value) => model.validateDescription(value),
    );
  }
}

class _MyFormState extends State<MyForm> {
  final _formKey = GlobalKey<FormState>();
  final NavigationService _navigationService = locator<NavigationService>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        // title: Text(
        //   widget._model.title,
        //   style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal),
        // ),
        automaticallyImplyLeading: false,
        leading: BackButton(),
        actions: [SaveButton(formKey: _formKey, model: widget._model)],
      ),
      body: Form(
        key: _formKey,
        autovalidateMode: AutovalidateMode.disabled,
        child: ListView(
          padding: EdgeInsets.all(16),
          children: <Widget>[
            SizedBox(
              height: 8,
            ),
            _ImagesList(
              widget: widget,
              navigationService: _navigationService,
            ),
            SizedBox(
              height: widget._model.images.isEmpty ? null : 8,
            ),
            Center(child: _AddImage(widget: widget)),
            SizedBox(
              height: 24,
            ),
            _NameInput(
              model: widget._model,
            ),
            SizedBox(
              height: 24,
            ),
            _InputDescription(
              model: widget._model,
            ),
            SizedBox(
              height: 24,
            ),
            TextFormField(
              initialValue: widget._model.providerTemp.telephone,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.phone),
                labelText: 'Teléfono',
                hintText: '00000000',
              ),
              inputFormatters: [
                MaskedInputFormater(
                  '#000-0000-0000',
                )
              ],
              keyboardType: TextInputType.phone,
              onSaved: (value) {
                widget._model.providerTemp.telephone = value.trim();
              },
              validator: (value) => widget._model.validatePhone(value),
            ),
            SizedBox(
              height: 24,
            ),
            TextFormField(
              initialValue: widget._model.providerTemp.email,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.mail),
                labelText: 'E-mail',
                hintText: 'alguien@mail.com',
              ),
              keyboardType: TextInputType.emailAddress,
              onSaved: (value) {
                widget._model.providerTemp.email = value.trim();
              },
              validator: (value) => widget._model.validateEmail(value),
            ),
            SizedBox(
              height: 24,
            ),
            TextFormField(
              textInputAction: TextInputAction.newline,
              initialValue: widget._model.providerTemp.address,
              decoration: InputDecoration(
                prefixIcon: Icon(Icons.place),
                labelText: 'Dirección',
                hintText:
                    'Alajuela, San Carlos, Ciudad Quesada, 300 norte de la terminal.',
              ),
              keyboardType: TextInputType.name,
              textCapitalization: TextCapitalization.sentences,
              maxLines: null,
              onSaved: (value) {
                widget._model.providerTemp.address = value.trim();
              },
            ),
            SizedBox(
              height: 24,
            ),
          ],
        ),
      ),
    );
  }
}

class _ImagesList extends StatelessWidget {
  const _ImagesList({
    Key key,
    @required this.widget,
    @required NavigationService navigationService,
  })  : _navigationService = navigationService,
        super(key: key);

  final MyForm widget;
  final NavigationService _navigationService;

  @override
  Widget build(BuildContext context) {
    return ReorderableWrap(
      spacing: 4.0,
      runSpacing: 4.0,
      maxMainAxisCount: 4,
      children: [
        for (final item in widget._model.images)
          Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
            ),
            key: ValueKey(item),
            width: MediaQuery.of(context).size.width / 4 - 12,
            child: GestureDetector(
              onTap: () async {
                widget._model.deleteImages =
                    await _navigationService.navigateTo(Routes.imageViewer,
                        arguments: ImageViewerArguments(
                            image: item,
                            images: widget._model.images,
                            deleteAction: true));

                widget._model.notifyListeners();
              },
              child: AspectRatio(
                aspectRatio: 4 / 3,
                child: item.saved
                    ? CachedNetworkImage(
                        fit: BoxFit.cover,
                        imageUrl: item.image.url,
                        placeholder: (context, url) =>
                            ImagePlaceHolder(icon: Icons.image),
                        errorWidget: (context, url, error) =>
                            ImagePlaceHolder(icon: Icons.broken_image),
                      )
                    : Image.file(
                        item.file,
                        fit: BoxFit.cover,
                      ),
              ),
            ),
          ),
      ],
      onReorder: (oldIndex, newIndex) {
        widget._model.moveImage(oldIndex: oldIndex, newIndex: newIndex);
      },
    );
  }
}

class _AddImage extends StatelessWidget {
  const _AddImage({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final MyForm widget;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // When we tap we call selectImage
      onTap: () {
        showModalBottomSheet(
            context: context,
            builder: (BuildContext bc) {
              return SafeArea(
                child: Container(
                  child: new Wrap(
                    children: <Widget>[
                      new ListTile(
                          leading: new Icon(Icons.photo_library),
                          title: new Text('Galería'),
                          onTap: () {
                            widget._model
                                .pickImage(source: ImageSource.gallery);
                            Navigator.of(context).pop();
                          }),
                      new ListTile(
                        leading: new Icon(Icons.photo_camera),
                        title: new Text('Cámara'),
                        onTap: () {
                          widget._model.pickImage(source: ImageSource.camera);

                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ),
              );
            });
      },
      child: Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
        width: MediaQuery.of(context).size.width / 4 - 12,
        child: AspectRatio(
          aspectRatio: 4 / 3,
          child: ImagePlaceHolder(
            icon: Icons.add_a_photo,
          ),
        ),
      ),
    );
  }
}

class _NameInput extends StatelessWidget {
  final ProviderEditViewModel model;
  const _NameInput({
    Key key,
    @required this.model,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: model.providerTemp.name,
      decoration: InputDecoration(
        labelText: 'Nombre',
        hintText: 'tecniherrajes',
      ),
      keyboardType: TextInputType.name,
      textCapitalization: TextCapitalization.sentences,
      onSaved: (value) {
        model.providerTemp.name = value.trim();
      },
      validator: (value) => model.validateName(value),
    );
  }
}
