import 'package:flutter/cupertino.dart';
import 'package:my_app_stacked/image/image.dart';
import 'package:my_app_stacked/image/utils/image_wrapper.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProviderViewViewmodel extends BaseViewModel {
  final NavigationService _navigation = locator<NavigationService>();

  ProviderModel _provider;

  List<ImageWrapper> images;

  ProviderViewViewmodel({ProviderModel provider}) {
    this._provider = provider;
    images = List<ImageWrapper>();

    addSevedImages(images: _provider.images);
  }

  ProviderModel get provider => this._provider;

  set setProvider(ProviderModel provider) {
    this._provider = provider;
  }

  String get urlFirstImage =>
      this._provider.images.isNotEmpty ? this._provider.images.first.url : "";

  ///Add saved images to [images] for show in view
  void addSevedImages({@required List<ImageModel> images}) {
    if (_provider.images != null && _provider.images.isNotEmpty) {
      _provider.images.forEach((image) {
        this.images.add(ImageWrapper(saved: true, image: image));
      });

      notifyListeners();
    }
  }

  imagesWrap(List<ImageModel> images) =>
      images.map((image) => ImageWrapper(image: image, saved: false)).toList();

  navigationBack() {
    _navigation.back();
  }

  navigationtoImageViewer(
      {@required ImageWrapper select, @required List<ImageWrapper> images}) {
    _navigation.navigateTo(Routes.imageViewer,
        arguments: ImageViewerArguments(
            image: select, images: images, deleteAction: false));
  }

  selectImage(ImageModel image) => ImageWrapper(image: image, saved: false);
}
