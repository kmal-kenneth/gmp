import 'package:auto_route/auto_route.dart';
import 'package:my_app_stacked/provider/provider.dart';
import 'package:my_app_stacked/locator/locator.dart';
import 'package:my_app_stacked/router/router.gr.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

class ProviderListViewmodel extends BaseViewModel {
  List<ProviderModel> providers;

  final NavigationService _navigationService = locator<NavigationService>();
  final ProviderRepositoryService providerRepository =
      ProviderRepositoryService();

  ProviderListViewmodel() {
    providers = List<ProviderModel>();
  }

  void listenToProviders() {
    providerRepository.providersRealTime().listen((result) {
      List<ProviderModel> updatedProviders = result;
      if (updatedProviders != null) {
        providers = updatedProviders;
        notifyListeners();
        print(
            '---------------- Articulos ${providers.length}---------------------');
        providers.forEach((element) {
          print('${element.name} - ${element.description} - ${element.images}');
        });
      }
    });
  }

  void requestMoreData() => providerRepository.requestMoreData();

  navigateToProviderEditView({ProviderModel provider}) async {
    await _navigationService.navigateTo(Routes.providerEditView,
        arguments: ProviderEditViewArguments(provider: provider));
    if (provider is ProviderModel) _navigationService.back();
  }

  navigateToProviderView({@required ProviderModel provider}) async {
    await _navigationService.navigateTo(Routes.providerView,
        arguments: ProviderViewArguments(provider: provider));
  }

  final DialogService _dialogService = locator<DialogService>();

  Future deleteProvider({@required ProviderModel provider}) async {
    var response = await _dialogService.showConfirmationDialog(
      title: 'Desea eliminar ${provider.name}?',
      description: 'Esta accion no se puede revertir.',
      confirmationTitle: 'Eliminar',
      dialogPlatform: DialogPlatform.Material,
      cancelTitle: 'Mantener',
    );

    if (response.confirmed) {
      setBusy(true);
      await providerRepository.removeProvider(provider: provider);
      setBusy(false);
    }

    _navigationService.back();

    notifyListeners();
  }
}
