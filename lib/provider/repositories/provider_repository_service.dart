import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:my_app_stacked/provider/provider.dart';

@lazySingleton
class ProviderRepositoryService {
  static const amountPerPage = 2;

  final CollectionReference _collectionReference =
      FirebaseFirestore.instance.collection('Providers');

  final StreamController<List<ProviderModel>> _providersController =
      StreamController<List<ProviderModel>>.broadcast();

  DocumentSnapshot _lastDocument;

  List<List<ProviderModel>> _allPagedResults = List<List<ProviderModel>>();

  bool _hasMorePosts = true;

  ProviderRepositoryService();

  /// Add [providerModel] to firestore
  Future addProvider({@required ProviderModel provider}) async {
    try {
      var result = await _collectionReference.add(provider.toMap());
      print(result);
      return true;
    } catch (e) {
      return e.toString();
    }
  }

  /// Buiild the query
  Query buildQuery({bool shortageProviders = false}) {
    Query pageQuery;

    if (shortageProviders == true) {
      pageQuery = _collectionReference
          .where('shortage', isEqualTo: true)
          .orderBy('name')
          .limit(amountPerPage);
    } else {
      pageQuery = _collectionReference.orderBy('name').limit(amountPerPage);
    }

    return pageQuery;
  }

  ///get providers
  Future fetchProviders() async {
    try {
      var documentSnapshot = await _collectionReference.get();
      if (documentSnapshot.docs.isNotEmpty) {
        return documentSnapshot.docs
            .map((snapshot) =>
                ProviderModel.fromMap(snapshot.data(), snapshot.id))
            .toList();
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///get providers
  Future fetchProvidersLimit({String orderBy = "name", int limit = 3}) async {
    try {
      var documentSnapshot =
          await _collectionReference.orderBy(orderBy).limit(limit).get();
      if (documentSnapshot.docs.isNotEmpty) {
        return documentSnapshot.docs
            .map((snapshot) =>
                ProviderModel.fromMap(snapshot.data(), snapshot.id))
            .toList();
      }
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///get provider by id
  Future<ProviderModel> getProviderById(String id) async {
    var doc = await _collectionReference.doc(id).get();
    return ProviderModel.fromMap(doc.data(), doc.id);
  }

  /// Listen providers in realtime
  Stream listenToProvidersRealTime({bool shortageProviders}) {
    _requestProviders(
      query: buildQuery(shortageProviders: shortageProviders),
    );

    return _providersController.stream;
  }

  ///remove providers from firestore
  Future removeProvider({@required ProviderModel provider}) async {
    await _collectionReference.doc(provider.id).delete();
  }

  ///Request more data from providers in firestore
  void requestMoreData() => _requestProviders(
      query: _collectionReference.orderBy('name').limit(amountPerPage));

  ///Updatye the provider in firestore
  Future updateProvider({@required ProviderModel provider}) async {
    try {
      await _collectionReference.doc(provider.id).update(provider.toMap());
    } catch (e) {
      if (e is PlatformException) {
        return e.message;
      }

      return e.toString();
    }
  }

  ///request the provider in paginate system
  void _requestProviders({
    @required Query query,
  }) {
    var pageQuery = query;

    if (_lastDocument != null) {
      pageQuery = pageQuery.startAfterDocument(_lastDocument);
    }

    if (!_hasMorePosts) {
      return;
    }

    var currentRequestIndex = _allPagedResults.length;

    pageQuery.snapshots().listen((snapshot) {
      if (snapshot.docs.isNotEmpty) {
        var providers = snapshot.docs
            .map((snapshot) =>
                ProviderModel.fromMap(snapshot.data(), snapshot.id))
            .toList();

        // Check if the page exists or not
        if (currentRequestIndex < _allPagedResults.length) {
          // If the page exists update the posts for that page
          _allPagedResults[currentRequestIndex] = providers;
        } else {
          // If the page doesn't exist add the page data
          _allPagedResults.add(providers);
        }

        // Concatenate the full list to be shown
        var allProviders = _allPagedResults.fold<List<ProviderModel>>(
            List<ProviderModel>(),
            (initialValue, pageItems) => initialValue..addAll(pageItems));

        //  Broadcase all providers

        _providersController.add(allProviders);

        // Save the last document from the results only if it's the current last page
        if (currentRequestIndex == _allPagedResults.length - 1) {
          _lastDocument = snapshot.docs.last;
        }

        // Determine if there's more posts to request
        _hasMorePosts = providers.length == amountPerPage;
      } else {
        // _providersController.add(List<ProviderModel>());
      }
    });
  }

  Stream providersRealTime({
    bool shortageProviders,
  }) {
    Query pageQuery;

    if (shortageProviders == true) {
      pageQuery = _collectionReference
          .where('shortage', isEqualTo: true)
          .orderBy('name');
    } else {
      pageQuery = _collectionReference.orderBy('name');
    }

    // Register the handler for when the posts data changes
    pageQuery.snapshots().listen((postsSnapshot) {
      var posts = postsSnapshot.docs
          .map(
              (snapshot) => ProviderModel.fromMap(snapshot.data(), snapshot.id))
          .toList();

      // Add the posts onto the controller
      _providersController.add(posts);
    });

    // Return the stream underlying our _postsController.
    return _providersController.stream;
  }
}
